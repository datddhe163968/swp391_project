<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">

         <!--Load fonts style after rendering the layout styles--> 
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
        <link rel="stylesheet" href="assets/css/main.css">


    </head>
    <body>
        <jsp:include page="header.jsp" />
        <!-- Ki?m tra v hi?n th? thng bo thnh cng -->

        <section class="login_box_area section_gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="login_box_img">
                            <img class="img-fluid" src="assets/img/login.jpg" alt="">
                            <div class="hover">
                                <h4>Have not an account?</h4>
                                <a class="btn btn-success" href="signup.jsp">Register</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 align-content-center"> 
                        <div class="login_form_inner ps-5 pe-5">
                            <h1 class="mb-5 text-center">Login</h1>
                            <form action="login" class="row signin-form" method="post">
                                <div class="form-group mb-3">
                                    <div class="d-flex  justify-content-start">
                                        <label for="username">Username</label>
                                    </div>
                                    <input type="text" id="username" name="username" class="form-control" required>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="d-flex justify-content-between">
                                        <label for="password">Password</label>
                                        <i onclick="hide()" class="fa-solid fa-eye" style="cursor: pointer"></i>
                                    </div>
                                    <input type="password" id="password" name="password" class="form-control" required>

                                </div>
                                <h5 class="text-danger">${sessionScope.mess}</h5>
                                <div class="form-group">
                                    <button type="submit" class="form-control btn btn-success">Log In</button>
                                </div>
                            </form>
                            <div class="mt-3 text-danger">${alert}</div>    
                            <div class="mt-3 text-success">${successMessage}</div>    
                            <div class="mt-5 d-flex justify-content-between">
                                <a class="highlight" href="resetpassword.jsp">Forgot Password</a>
                                <p class="text-center text-md-right">Not a member? <a class="highlight" data-toggle="tab" href="signup.jsp">Sign Up</a></p>                                     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            function hide(){
                var passwordElement = document.getElementById("password");
                var eyeIcon = document.querySelector("i");
                console.log(eyeIcon);
                if(passwordElement.type === 'text'){
                    eyeIcon.classList.toggle("fa-eye");
                    eyeIcon.classList.toggle("fa-eye-slash");
                    passwordElement.type = 'password';
                }else{
                    eyeIcon.classList.toggle("fa-eye");
                    eyeIcon.classList.toggle("fa-eye-slash");
                    passwordElement.type = 'text';
                }
            }
        </script>
    </body>
    <%@include file="footer.jsp" %>                                                                                                                                                                                                                                        
</html>
