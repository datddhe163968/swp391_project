<%-- 
    Document   : MyRegistrations
    Created on : May 23, 2023, 3:22:29 PM
    Author     : quang
--%>

<%@ page import="java.util.List" %>
<%@page import= "model.CourseLesson" %>
<%@page import= "DAO.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%    
Integer courseid = (Integer) request.getAttribute("courseid");

    
    LessonDAO u = new LessonDAO();


    List<CourseLesson>  lesson1 = (List<CourseLesson>) request.getAttribute("lesson");

    // Thực hiện các lệnh khác ở đây nếu lessonList đã được thiết lập

   
%>
<html>
    <head>

        <title>Subjects List</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">


        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
 <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <style>

            </style>
                    <link rel="stylesheet" type="text/css" href="assets/css/lessondetail.css">
                    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.15/dist/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.15/dist/sweetalert2.min.js"></script>


        </head>
        <%if(request.getSession().getAttribute("user")==null)
                   response.sendRedirect("forbiddenAlert.jsp");
        %>
        <c:if test="${sessionScope.roleName.equals('admin') || sessionScope.roleName.equals('expert')}">
            <body>
                <%@include file="header.jsp" %>
                <main class="ttr-wrapper">
                    <div class="container-fluid">
                        <div class="navbar3" style="  background-color: #f9f9f9;
                             border-bottom: 1px solid #e5e5e5;
                             padding-top: 10px;
                             padding-bottom: 10px;
                        }">
                        <div class="container2">
                            <span class="navbar3-brand"><a href="home">Home</a></span>
                            <span class="navbar3-brand-divider ">/</span>
                            <span class="navbar3-brand"><a href="sublist">Subject Management</a></span>
                             <span class="navbar3-brand-divider ">/</span>
                   <span class="navbar3-brand"><a href="sublesson?courseid=${course}">Lessons Management</a></span>
                             <span class="navbar3-brand-divider ">/</span>
                   <span class="navbar3-brand"><a href="lessondetail?bid=${id}&course=${course}&Type=${Type}&model=1">Lessons Detail</a></span>


                        </div>
                    </div>
                   <div class="d-flex justify-content-center">
                  
                    <div class="row w-75">
                        <!-- Your Profile Views Chart -->
                        <div class="col-lg-12 m-b30">
                            <div class="widget-box">

                                <div class="manage b" style="text-align: center;">
                                    <h1>Lesson Detail</h1>
<!--                                    <button class="add-subject">Add new Subject</button>-->
                                </div>


<!--                                <div class="search-bar">
                                    <form action = "search" method="post" style=" display: flex; align-items: center;">
                                        <input type="hidden" name ="id" value="<%=courseid%>">

                                        <input type="text" name ="name" placeholder="Subject Name...">
                                        <button type="submit"><i>Search</i></button>
                                    </form>

                                    <form action="catogory" method="get" style="width: 500px">
                                        <select class="col-6" name="categoryID" onchange="this.form.submit()">
                                            <option value="" disabled selected hidden>choose category</option>
                                                <option value="">all</option>

                                            <c:forEach var="i" items="${requestScope.lesson}">
                                                <option value="${i.getLessonId()}" ${i.getLessonId() == param.categoryID ? 'selected' : ''}>${i.getLessonTopic()}</option>
                                            </c:forEach>

                                        </select>
                                    </form>
                                    <form action="status" method="get" style="width: 500px">
                                         <input type="hidden" name ="name" value="<%=courseid%>">
                                        <select class="col-4" name="sta" onchange="this.form.submit()">
                                            <option value="" disabled selected hidden>Status</option>
                                            <option value="1" >Active</option>
                                            <option value="0" >Deactive</option>
                                        </select>
                                    </form>
                                </div>-->


                               <div class="widget-inner" id="subjectlist">
                                   <c:if test="${not empty sessionScope.successMessage}">
  <div style="background-color: green; color: white; text-align: center;">${sessionScope.successMessage}</div>
</c:if>
                                   <div class="card-courses-media" style="width: 100%; height:500px;">
  <div style="height:500px; padding-bottom:56.25%;">
    <iframe style="top:0; left:0; width:100%; height:500px;" src="${lessondetail.getVideoLink()}" frameborder="0" allowfullscreen></iframe>
  </div>
</div>
   <c:if test="${Type == 1}">
        <div class="card-courses-full-dec">
                                       <div class="card-courses-title" >
                                                <h4 style=" font-size: 30px;">${lessondetail.getLessonName() }</h4>
                                            </div>
                                            <div class="card-courses-list-bx">
                                                        <div class="card-courses-user-info">
                                                            <h5 style=" font-size: 20px; text-decoration: none;">${lessondetail.getLessonTopic() }</h5>
                                                        </div>
                                                   
                                            </div>
                                            <div class="card-courses-list-bx">
                                                <ul class="card-courses-view">
                                                      
                                                </ul>
                                            </div>

                                                    <div class="row card-courses-dec">
                                                        <div class="col-md-12">
                                                            <h6 class="m-b10" style=" font-size: 20px;">Lesson Description</h6>
                                                            <p style=" font-size: 20px;"> ${lessondetail.getContent() }</p>
                                                        </div>
                                                        <div class="col-md-12">


                                                        </div>
                                                    </div>
                                                        <button class="edit-lesson" style="margin: 10px 700px;  background-color:green;  color: white;"  >Edit</button>
                                        </div>
    </c:if>
   <c:if test="${Type == 2}">
                                                               <button class="edit-lesson" style="margin: 10px 700px;  background-color:green;  color: white;"  >Edit</button>

        </c:if>
                               
  
</div>
  <div id="success-message" style="display: none; background-color: green; color: white; text-align: center;">Update thành công</div>
                                                        <div class="hidden" id="hidden" style="display: none;">
 
  <div>
      <form action="lessondetail" method="post" onsubmit="return validateForm()" enctype="multipart/form-data"  >
          <div>
              <label>Lesson Type:</label>
                   <c:if test="${lessondetail.lessonTypeId == 1}">
                       <input type="text" name="" value="Video" readonly="">

    </c:if>
                       <c:if test="${lessondetail.lessonTypeId == 2}">
                       <input type="text" name="" value="HTML" readonly="">

    </c:if>
                       <c:if test="${lessondetail.lessonTypeId == 3}">
                       <input type="text" name="" value="Quiz" readonly="">

    </c:if>
         
                 
          </div>
            <div  id="videolink-div">
                <label>Video Link:</label>
               <input type="text" name="videolink" value="${lessondetail.getVideoLink() }" required oninput="if (this.validity.patternMismatch) this.setCustomValidity('Vui lòng đúng định dạng: https://www.youtube.com/embed/'); else this.setCustomValidity('')" pattern="https:\/\/www.youtube.com\/embed\/.+">
            </div>
            <c:if test="${Type == 2}">
              <div id="htmlfile-div">
              <label>HTML File:</label>
              <input type="file" name="html"  accept=".html" onchange="checkFileSize(event)" maxlength="10000000"  >
          </div>

        </c:if>
                 
          <div class="card-courses-title">
              <label>Lesson Name:</label>
              <input type="text" name="lessonname" value="${lessondetail.getLessonName() }" required >
          </div>
          <div class="card-courses-list-bx">
              <div class="card-courses-user-info">
                  <label>Lesson Topic:</label>
                  <input type="text" name="lessontopic"  value="${lessondetail.getLessonTopic() }" required>
                  <input type="hidden" name="id"  value="${lessondetail.getLessonId() }">
                  <input type="hidden" name="course"  value="${course}">
                   <input type="hidden" name="referer" value="<%=request.getRequestURL().toString()%>">

              </div>
          </div>
          <div class="card-courses-list-bx"></div>
          <div class="row card-courses-dec">
              <div class="col-md-12">
                  <label>Lesson Description:</label>
              </div>
          </div>
          <div class="row card-courses-dec">
              <div class="card-courses-user-info">
                  <textarea name="lessoncontent" maxlength="1000" >${lessondetail.getContent() } </textarea >
              </div>
          </div>
          <div style=" display: flex;
    justify-content: center;
    align-items: center;
    margin: 0;">
             <button class="btn button-sm green radius-xl border" type="submit" name="action" value="save"  onclick="saveData()">Save</button>
              <span class="navbar3-brand" style=" " ><a href="lessondetail?bid=${id}&course=${course}&Type=${Type}&model=1" class="btn button-sm green radius-xl border" style="background-color: green; color: white">Cancel</a></span>

          </div>

      </form>
               </div>
             
  </div>
</div>

                            </div>
                        </div>
                        <!-- Your Profile Views Chart END-->
                    </div>
                   
                   </div>
                </div>
                            
                    
            </main>

            <%@include file="footer.jsp" %>

        </body>
         <script src="assets/js/lessondetail.js"></script>
            <script>
      var lessonType = ${lessondetail.lessonTypeId};
      var videolinkDiv = document.getElementById("videolink-div");

      if (lessonType == 2 || lessonType == 3) {
        videolinkDiv.style.display = "none";
      } else {
        videolinkDiv.style.display = "block";
      }
   function previewImage(event) {
    var input = event.target;
    var reader = new FileReader();
    var preview = document.getElementById('preview');

    if (input.files && input.files[0]) {
      reader.onload = function() {
        preview.src = reader.result;
      };
      reader.readAsDataURL(input.files[0]);
    } else {
      preview.src = '${detail.image}';
    }
  }
    </script>
    <script>
      var subjectlist = document.getElementById("subjectlist");
      var hidden = document.getElementById("hidden");
      var editButton = document.querySelector(".edit-lesson");

      editButton.addEventListener("click", function() {
        subjectlist.style.display = "none";
        hidden.style.display = "block";
      });
    </script>
    <script>
        function checkFileSize(event) {
            var file = event.target.files[0];
            if (file.size > 10000000) {
                alert("File HTML quá lớn, vui lòng chọn tệp tin HTML có kích thước nhỏ hơn 10MB.");
                event.target.value = "";
            }
        }
   
    </script>

    <script>
    function saveData() {
      // Check if all required fields are filled in
      if (!document.getElementsByName('lessonname')[0].value||!document.getElementsByName('html')[0].value || !document.getElementsByName('lessontopic')[0].value || !document.getElementsByName('lessoncontent')[0].value) {
        // Display a warning message using SweetAlert2
        Swal.fire({
          title: 'HTML not empty!',
          icon: 'warning',
          showConfirmButton: false,
          timer: 2000
        });
        // Prevent the form submission
        return false;
      }

      // Save data to the database

      // Display success message using SweetAlert2
      Swal.fire({
        title: 'Data saved successfully!',
        icon: 'success',
        showClass: {
          popup: 'animate__animated animate__fadeInDownBig'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUpBig'
        },
        timer: 2000,
        timerProgressBar: true,
        showConfirmButton: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
          setTimeout(() => {
            Swal.resumeTimer()
          }, 200)
        }
      });
       document.querySelector('button[name="action"][value="save"]').disabled = true;

      // Wait for 5 seconds before submitting the form
      setTimeout(() => {
        // Submit the form
        document.querySelector('form').submit();
      }, 2000);

      // Prevent the form submission immediately
      return false;
    }
    </script>

    </c:if>
        
</html>
