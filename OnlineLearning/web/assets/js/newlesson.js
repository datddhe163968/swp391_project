/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


        function toggleVideoLink() {
            var lessonType = document.getElementsByName("lesson-type")[0].value;
            var videoLinkDiv = document.getElementById("videolink-div");
            var htmlFileDiv = document.getElementById("htmlfile-div");
            if (lessonType == "2") {
                videoLinkDiv.style.display = "none";
                htmlFileDiv.style.display = "block";
            } else {
                videoLinkDiv.style.display = "block";
                htmlFileDiv.style.display = "none";
            }
        }
        function checkFileSize(event) {
            var file = event.target.files[0];
            if (file.size > 10000000) {
                alert("File HTML quá lớn, vui lòng chọn tệp tin HTML có kích thước nhỏ hơn 10MB.");
                event.target.value = "";
            }
        }
        // Validate form
        function validateForm() {
            // Get form fields
            const lessonType = document.querySelector('select[name="lesson-type"]').value;
            const status = document.querySelector('select[name="status"]').value;
            const videoLink = document.querySelector('input[name="videolink"]').value;
            const htmlFile = document.querySelector('input[name="html"]').value;
            const lessonName = document.querySelector('input[name="lessonname"]').value;
            const lessonTopic = document.querySelector('input[name="lessontopic"]').value;
            const lessonContent = document.querySelector('textarea[name="lessoncontent"]').value;

            // Check if all fields are filled in
            if (!lessonType || !status || !lessonName || !lessonTopic || !lessonContent) {
                Swal.fire({
                    title: 'Please fill in all fields',
                    icon: 'error'
                });
                return false;
            }

            // Check if video link is valid if lesson type is video
            if (lessonType === '1' && !videoLink.match(/^https:\/\/www.youtube.com\/embed\/.+$/)) {
                Swal.fire({
                    title: 'Please enter a valid YouTube video link',
                    icon: 'error'
                });
                return false;
            }

            // Check if HTML file is selected if lesson type is HTML
            if (lessonType === '2' && !htmlFile) {
                Swal.fire({
                    title: 'Please select an HTML file',
                    icon: 'error'
                });
                return false;
            }
            document.querySelector('button[name="action"][value="save"]').disabled = true;

            // Wait for 5 seconds before submitting the form
            setTimeout(() => {
                // Submit the form
                document.querySelector('form').submit();
            }, 3000);
            saveData();

            // Prevent the form submission immediately
            return false;
            // All fields are valid, save data
            return true;

        }

        // Save data and show success message
        function saveData() {
            // Save data to database

            // Show success message
            Swal.fire({
                title: 'Add Lesson successfully!',
                icon: 'success',
                showClass: {
                    popup: 'animate__animated animate__fadeInDownBig'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUpBig'
                },
                timer: 3000,
                timerProgressBar: true,
                showConfirmButton: false,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

        }

