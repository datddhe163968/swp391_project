document.addEventListener('DOMContentLoaded', function () {
    var placeholders = document.querySelectorAll('.video-placeholder');
    var popupWindow = null;

    for (var placeholder of placeholders) {
        var buttonElement = placeholder.querySelector('button');
        buttonElement.addEventListener('click', function (event) {
            var buttonParent = event.target.parentElement;
            var videoLink = buttonParent.getAttribute('data-video');
            event.preventDefault();
            if (!popupWindow || popupWindow.closed) {
                var left = window.screenLeft || window.screenX || 0;
                var top = window.screenTop || window.screenY || 0;
                var width = 800; // Chiều rộng của cửa sổ pop-up
                var height = 600; // Chiều cao của cửa sổ pop-up

                var popupLeft = window.innerWidth / 2 - width / 2 + left;
                var popupTop = window.innerHeight / 2 - height / 2 + top;
                popupWindow = window.open(videoLink, 'videoPopup', 'left=' + popupLeft + ',top=' + popupTop + ',width=' + width + ',height=' + height);
                buttonElement.textContent = 'Watch Video';
            } else {
                popupWindow.close();
                buttonElement.textContent = 'Watch Video';
            }
        });
    }
});

function display(id) {
    var lessonBoxes = document.querySelectorAll(".lesson-box");

    for (var lessonBox of lessonBoxes) {
        if (id === "lesson-0") {
            lessonBox.style.display = 'block';
        } else {
            console.log(lessonBox.getAttribute("id"));
            if (lessonBox.getAttribute("id") === id) {
                lessonBox.style.display = 'block';
            } else {
                lessonBox.style.display = 'none';
            }
        }
    }
}


var chapterBoxes = document.querySelectorAll(".chapter-box");
for (var chapterBox of chapterBoxes) {
    chapterBox.addEventListener("click", (e) => {
        var id = e.target.getAttribute("id");
        display(id);
    });
}