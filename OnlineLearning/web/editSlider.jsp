<%-- 
    Document   : addslider
    Created on : Mar 15, 2023, 2:11:03 PM
    Author     : thaib
--%>

<!DOCTYPE html>
<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <head>
        <title>Edit Slider</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/main.css">

        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/nouislider.min.css">
        <link rel="stylesheet" href="assets/css/ion.rangeSlider.css" />
        <link rel="stylesheet" href="assets/css/ion.rangeSlider.skinFlat.css" />
        <link rel="stylesheet" href="assets/css/magnific-popup.css">



        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    </head>
    <%if(request.getSession().getAttribute("user")==null)
                   response.sendRedirect("forbiddenAlert.jsp");
    %>
    <c:if test="${sessionScope.roleName.equals('marketer')}">

        <body>

            <!-- Start Header Area -->
            <%@ include file = "header.jsp" %>
            <!-- End Header Area -->



            <section class="blog_area">
                <div class="container">
                    <h1 class='m-4'>Edit Slider</h1>
                    <div class="card p-3 mb-4 d-flex justify-content-center">
                        <form class='d-flex justify-content-center' action="EditSlider?id=${slider.getId()}" method="POST" enctype='multipart/form-data'>
                            <div>

                                <article class="row blog_item">

                                    <div>
                                        <div class='mb-5' id="displayImg"><img width="1000px" src="${slider.getUrlImage()}" alt=""></div>
                                        <div class='d-flex'>
                                            <div class='w-50'>
                                                <div class="blog_post">
                                                    <label for="image"><p>Image:</label>
                                                    <input type="file" accept=".jpg, .png" name="image" id="uploadfile" onchange="ImagesFileAsURL()"/>
                                                </div>
                                                <label for="status"><p>Status:</label>
                                                <select class='form-select' name="status">
                                                    <option value="true">Show</option>
                                                    <option value="false">Hide</option>
                                                </select>
                                                <label for="category"><p>Category:</label>
                                                <select class='form-select' name="catname">
                                                    <c:forEach items="${category}" var="c">
                                                        <option <c:if test="${slider.getCategory() == c.getCatergoryName()}">selected</c:if> value="${c.getCatergoryName()}">${c.getCatergoryName()}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class='w-50'>
                                                <label for="title">BackLink:</label>
                                                <input class='form-control mb-3' type="text" name="backlink" value="${slider.getBackLink()}">
                                                <label for="title">Title:</label>
                                                <input class='form-control mb-3' type="text" name="title" value="${slider.getTitle()}">
                                                <label for="note">Note:</label>
                                                <input class='form-control mb-3' type="text" name="note" value="${slider.getNote()}">
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <div class="row">
                                    <div class="col-md-12 mt-2 text-center">
                                         <a href="SliderList" class="btn btn-light">Cancel</a>
                                        <button class="btn btn-primary btn-success" type="submit" onclick="return alert('Edit successfully')">Save</button>
                                        <input class="btn btn-secondary" type="reset" value="Enter again">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>




            <!-- start footer Area -->
            <%@include file="footer.jsp" %>
            <!-- End footer Area -->
            <script type="text/javascript" >
                function ImagesFileAsURL() {
                    var fileSelected = document.getElementById('uploadfile').files;
                    if (fileSelected.length > 0) {
                        var filetoLoad = fileSelected[0];
                        var fileReader = new FileReader();
                        fileReader.onload = function (fileLoaderEvent) {
                            var srcData = fileLoaderEvent.target.result;
                            var newImage = document.createElement('img');
                            newImage.src = srcData;
                            document.getElementById('displayImg').innerHTML = newImage.outerHTML;
                        };
                        fileReader.readAsDataURL(filetoLoad);
                    }
                }
                

            </script>

        </body>

    </c:if>
</html>
