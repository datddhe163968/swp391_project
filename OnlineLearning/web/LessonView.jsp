<%@ page import="java.util.List" %>
<%@page import= "model.*" %>
<%@page import= "DAO.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%    
    Account user = (Account) request.getSession().getAttribute("user");
    if(user==null){
        response.sendRedirect("forbiddenAlert.jsp");
    }
    String Courseid = request.getParameter("courseid");
    int course = Integer.parseInt(Courseid);
    LessonDAO u = new LessonDAO();
    List<CourseLesson> lessonList = u.getByCourseId(course);
    int lessonCount = u.CountLesson(course);
    String courseName = u.GetCourseName(course);
    String courseImg = u.getCourseImg(course);
%>
<html>
    <head>
        <title>Lesson view</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/LessonView.css">


        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <!--
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
                      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">-->
    </head>
    <body>
        <%@ include file="header.jsp" %>
        <div class="navbar-link">
            <span><a href="home">Home</a></span>
            <span class="ms-2 me-2">/</span>
            <span><a href="myregistation?user-id=<%=user.getId()%>">My Registrations</a></span>
            <span class="ms-2 me-2">/</span>
            <span>Lesson view</span>
        </div>

        <!-- <%= Courseid %> -->

        <div class="row p-5 pt-3">
            <div class="col-md-4">
                <div class="card bg-light mb-3">
                    <div class="sidebar">
                        <div class="widget">
                            <h2 class="mt-3 widget-title" style="text-align: center"><%=courseName%> Lesson</h2>
                            <p style="text-align: center"><strong>Number of lesson: <%=lessonCount%></strong></p>
                            <img src="<%=courseImg%>" class="course-img" alt="Course Picture">

                            <div class="mt-5">
                                <div id="lesson-0" class="chapter-box shadow-sm lesson-title">ALL</div>
                                <%
                                    List<String> chapterNames = u.getChapterFromLessonList(lessonList);
                                    int j = 1;
                                    for(String chapterName : chapterNames){
                                %>
                                <div id="lesson-<%=j%>" class="chapter-box shadow-sm lesson-title"><%=chapterName.toUpperCase()%></div>
                                    <%
                                        j++;}
                                    %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 card bg-light">
                <div class="ps-5 pe-5 pt-3 pb-3">

                    <% 
                        int i = 1;
                        UserAnswerDAO uAD = new UserAnswerDAO();
                        uAD.clearResult();
                    %>
                    <% for (CourseLesson lesson : lessonList) {
                        int resultId = uAD.getNewestResultId(user.getId(),lesson.getQuizId());
                            
                    %>
                    <div id="lesson-<%=lesson.getLessonOrder()%>" class="lesson-box shadow rounded">
                        <% if (lesson.getLessonTypeId() == 1) { %>
                        <div class="video-container">
                            <h5 class="lesson-title"><%= lesson.getLessonName() %></h5>
                            <%= lesson.getContent() %><p>
                            <div class="video-placeholder d-flex justify-content-end" data-video="<%= lesson.getVideoLink() %>">
                                <button class="btn btn-primary btn-danger">Watch Video</button>
                            </div>
                        </div>
                        <% } else if (lesson.getLessonTypeId() == 2) { %>
                        <h5 class="lesson-title"><%= lesson.getLessonName() %></h5>
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-primary btn-danger" onclick="window.location.href = '<%= lesson.getVideoLink() %>'">View the HTML</button>
                        </div>
                        <% } else { %>
                        <h5 class="lesson-title"><%= lesson.getLessonName() %></h5>
                        <div class="d-flex justify-content-end">
                            <a href="quizlesson?resultId=<%=resultId%>&quizId=<%=lesson.getQuizId()%>">
                                <button class="btn btn-primary btn-danger">Quiz Details</button>
                            </a>
                        </div>

                        <!--<button class="watch-video-button" onclick="window.location.href = '#'">Take the quiz</button>-->
                        <% } %>
                    </div>
                    <% i++; %>
                    <% } %>
                </div>
            </div>
        </div>


        <%@include file="footer.jsp" %>  
        <script src="assets/js/LessonView.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>
</html>
