<%-- 
    Document   : homepage
    Created on : May 16, 2023, 11:48:04 PM
    Author     : quang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "java.util.*" %>
<%@page import = "DAO.*" %>
<%@page import = "model.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Homepage</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">

        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/detective.css">
        <link rel="stylesheet" href="assets/css/hot.css">
        <link rel="stylesheet" href="assets/css/flag.css">
        <link rel="stylesheet" href="assets/css/home.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
        <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    </head>

</head>
<%            
    if(request.getSession().getAttribute("user")==null){
               request.getSession().setAttribute("roleName", "guest");
        }
        CourseDAO dao = new CourseDAO();
    List<Course> listfeatureCourses = dao.getCourses();
%>
<body class="vh-100">
    <%@include file="header.jsp" %>
</body>   
<section class="slider">
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <c:forEach items="${listSliders}" var="slider" varStatus="i">
                <c:if test="${slider.getStatus() eq true}">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="${i.index}" <c:if test="${i.index==0}"> class="active" aria-current="true"</c:if> aria-label="Slide ${i.index}"></button>
            </c:if>
            </c:forEach>
        </div>
        <div class="carousel-inner">
            <c:forEach items="${listSliders}" var="slider" varStatus="i">
                <c:if test="${slider.getStatus() eq true}">
                <div class="carousel-item ${i.index==0?'active':''}">
                    <a href="${slider.getBackLink()}"><img src="${slider.getUrlImage()}" class="d-block w-100" style="height:450px" alt="..." /></a>
                </div>
                </c:if>
            </c:forEach>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true" ></span>
            <span style="visibility: hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span style="visibility: hidden">Next</span>
        </button>
    </div>
</section>

<section class="category-area mb-4">
    <div class="row text-center mb-3">
        <h1 style="margin-top: 200px; margin-bottom: -70px;">Hot Courses</h1>
    </div>              
    <div class="container d-flex align-items-center justify-content-center" style="height: 100vh;">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-4 d-flex flex-column align-items-center justify-content-center">
                <img src="assets/img/sachli.png" alt="Image 1" class="img-fluid zoom-image">
                <h4 class="text-center mt-1">PhysicsCourse</h4>
                <h5 class="text-center mt-1" style="margin-bottom: 10px;">Published on March 1</h5>
                <a href="coursedetail?courseid=3" class="btn btn-success mt-1">View Details</a>
            </div>

            <div class="col-lg-4 col-md-4 d-flex flex-column align-items-center justify-content-center">
                <img src="assets/img/sachsinh.png" alt="Image 2" class="img-fluid zoom-image">
                <h4 class="text-center mt-1">BiologyCourse</h4>
                <h5 class="text-center mt-1">Published on May 30</h5>
                <a href="coursedetail?courseid=5" class="btn btn-success mt-1">View Details</a>                  
            </div>
            <div class="col-lg-4 col-md-4 d-flex flex-column align-items-center justify-content-center">
                <img src="assets/img/sachnhac.png" alt="Image 3" class="img-fluid zoom-image">
                <h4 class="text-center mt-1">MusicCourse</h4>
                <h5 class="text-center mt-1">Published on April 23</h5>
                <a href="coursedetail?courseid=4" class="btn btn-success mt-1">View Details</a>

            </div>
        </div>

    </div>
    <div style="display: flex; justify-content: center;">
        <div class="center mt-1" style="transform: scale(2); margin-bottom: 10px;">
            <span class="flag-icon" style="margin-top: 50px;">🚩</span>
            <span class="flag-text" style="margin-top: 50px;">Feature Course</span>
        </div>
    </div>

    <div class="container d-flex justify-content-center flex-wrap" style="margin-top: 10px;">
        
        <c:forEach items="${listfeaturCourses}" var="course">

            <c:if test="${course.getFeatureflag() eq 1}">
                <div class="col-lg-4 col-md-4 d-flex align-items-center justify-content-center flex-wrap">

                    <div class="feature shadow d-flex justify-content-center align-items-center">
                        <div class="boxcontent text-center justify-content-center">
                            <img src="${course.image}" alt="Course Image" class="img-fluid zoom-image">
                            <p class="text-center justify-content-center">${course.courseName}</p>
                        </div>
                    </div>
                </div>

            </c:if>

        </c:forEach>


    </div>
    





</section>


<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick.css" />
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">

</script>
<script>
    $(document).ready(function () {
        $('.slider').slick({
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    });
</script>
<%@include file="footer.jsp" %>
</html>
