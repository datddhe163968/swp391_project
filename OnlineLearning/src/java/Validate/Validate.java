/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author dell
 */
public class Validate {
    private static Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_REGEX = "^[A-Za-z0-9]+[A-Za-z0-9]*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)$";
    private static String name_regex = "^[A-Z][a-zA-Z]{1,}$";
    private static String user_name_regex = "^[a-zA-Z][a-zA-Z\\d]+$";
    private static String password_regex = "^.{6,}$";
    
    public Validate() {
        pattern = Pattern.compile(EMAIL_REGEX);
    }

    public boolean validateEmail(String regex) {
        matcher = pattern.matcher(regex);
        return matcher.matches();
    }
    public boolean validateName(String regex){
        boolean check = regex.matches(name_regex);
        return check;
    }
    public boolean validateUserName(String regex){
        boolean check = regex.matches(user_name_regex);
        return check;
    }
    
    public boolean validatePassword(String regex){
        boolean check = regex.matches(password_regex);
        return check;
    }

    public boolean checkPhone(String str) {
        String reg = "^^0(?:\\d{8}|\\d{9})$";
        boolean kt = str.matches(reg);
        return kt;
    }
    
    public static void main(String[] args) {
        Validate v = new Validate();
        System.out.println(v.validateEmail("tunqhe@fpt.edu.vn"));
    }
    }

