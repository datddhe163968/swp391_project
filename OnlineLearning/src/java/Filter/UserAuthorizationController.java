/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Filter;

import DAO.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author quang
 */
public class UserAuthorizationController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        RoleDAO r = new RoleDAO();
        Account u = (Account) session.getAttribute("user");
        String role = null;
        if (u != null) {
            role = r.getRoleNameByUserId(u.getId());
        }
        if (role == null) {
            role = "Guest";
        }
        if (role.equals("Guest") || role.equals("Customer") || role.equals("Expert")) {
            req.getRequestDispatcher("home").forward(req, resp);
        } else if(role.equals("Admin")) {
            req.getRequestDispatcher("admindashboard.jsp").forward(req, resp);
        } else{
            req.getRequestDispatcher("sellerdashboard.jsp").forward(req, resp);
        }
    }

}
