/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author DAT
 */
public class LessonType {
  private int LessonTypeId;
  private String LessonTypeName;

    public LessonType(int LessonTypeId, String LessonTypeName) {
        this.LessonTypeId = LessonTypeId;
        this.LessonTypeName = LessonTypeName;
    }

    public LessonType() {
    }

    public int getLessonTypeId() {
        return LessonTypeId;
    }

    public void setLessonTypeId(int LessonTypeId) {
        this.LessonTypeId = LessonTypeId;
    }

    public String getLessonTypeName() {
        return LessonTypeName;
    }

    public void setLessonTypeName(String LessonTypeName) {
        this.LessonTypeName = LessonTypeName;
    }

    @Override
    public String toString() {
        return "LessonType{" + "LessonTypeId=" + LessonTypeId + ", LessonTypeName=" + LessonTypeName + '}';
    }
  
}
