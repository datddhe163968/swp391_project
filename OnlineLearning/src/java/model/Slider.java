/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Slider {
    private int Id;
    private String UrlImage;
    private String Category;
    private String BackLink;
    private boolean Status;
    private String Note;
    private String Title;

    public Slider() {
    }

    public Slider(int Id, String UrlImage, String Category, String BackLink, boolean Status, String Note, String Title) {
        this.Id = Id;
        this.UrlImage = UrlImage;
        this.Category = Category;
        this.BackLink = BackLink;
        this.Status = Status;
        this.Note = Note;
        this.Title = Title;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getUrlImage() {
        return UrlImage;
    }

    public void setUrlImage(String UrlImage) {
        this.UrlImage = UrlImage;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public String getBackLink() {
        return BackLink;
    }

    public void setBackLink(String BackLink) {
        this.BackLink = BackLink;
    }

    public String isStatus() {
        return Status ? "Show" : "Hide";
    }
    public boolean getStatus(){
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String Note) {
        this.Note = Note;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }
    
    

}