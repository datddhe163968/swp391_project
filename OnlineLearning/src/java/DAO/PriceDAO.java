/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import model.Blog;
import model.Course;
import model.Price;

/**
 *
 * @author DAT
 */
public class PriceDAO extends MyDAO {

    public List<Price> getAll() {
        List<Price> list = new ArrayList<>();
        xSql = "select * from PricePackage ";

        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int pPackageId;
            String pPackageName;
            int pDuration;
            boolean pstatus;
            double pListprice;
            double pSaleprice;
            String pDescription;
            int pCourseId;
            while (rs.next()) {
                pPackageId = rs.getInt("PackageId");
                pPackageName = rs.getString("PackageName");
                pDuration = rs.getInt("Duration");
                pstatus = rs.getBoolean("status");
                pListprice = rs.getDouble("Listprice");
                pSaleprice = rs.getDouble("Saleprice");
                pDescription = rs.getString("Description");
                pCourseId = rs.getInt("CourseId");
                // create object
                Price price = new Price(pPackageId, pPackageName, pDuration, pstatus, pListprice, pSaleprice, pDescription, pCourseId);
                list.add(price);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void InsertPrice(String name, String duration, boolean status, Double listprice, Double saleprice, String description) {
        String sql = "INSERT INTO [Nhom3].[dbo].[PricePackage] \n"
                + "([PackageName], [Duration], [Status], [ListPrice], [SalePrice], [Description] )\n"
                + "VALUES (?,?,?,?,?,?);";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, duration);
            ps.setBoolean(3, status);
            ps.setDouble(4, listprice);
            ps.setDouble(5, saleprice);
            ps.setString(6, description);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Price> getListByPage(List<Price> list,
            int start, int end) {
        ArrayList<Price> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public void editPrice(String PackageName, String duration, boolean status, Double listprice, Double saleprice, String description, int PackageId) {
        String sql = "  UPDATE [Nhom3].[dbo].[PricePackage]\n"
                + "SET [PackageName] = ?,\n"
                + "    [Duration] = ?,\n"
                + "    [Status] = ?,\n"
                + "    [ListPrice] = ?,\n"
                + "    [SalePrice] = ?,\n"
                + "    [Description] = ?\n"
                + "WHERE [PackageId] = ?;";
        try{
            ps = con.prepareStatement(sql);
            ps.setString(1, PackageName);
            ps.setString(2, duration);
            ps.setBoolean(3, status);
            ps.setDouble(4, listprice);
            ps.setDouble(5, saleprice);
            ps.setString(6, description);
            ps.setInt(7, PackageId);
            ps.executeUpdate();
        }catch (Exception e) {
        }
    }

    public Price selectById(int t){
        Price ketqua = null;
        xSql = "select * from PricePackage where PackageId = ?";
        try{
            ps = con.prepareStatement(xSql);
            ps.setInt(1,(t));
            rs = ps.executeQuery();
            if (rs.next()) {
                int PackageId = rs.getInt("PackageId");
                String PackageName = rs.getString("PackageName");
                int Duration = rs.getInt("Duration");
                boolean Status = rs.getBoolean("Status");
                double Listprice = rs.getInt("ListPrice");
                double Saleprice = rs.getInt("SalePrice");
                String Description = rs.getString("Description");               
                int CourseId = rs.getInt("CourseId");
                ketqua = new Price(PackageId, PackageName, Duration, Status, Listprice, Saleprice, Description, CourseId);
                
                
                
        } else {
                ketqua = null;
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return (ketqua);
    }
    
    public void deletePrice(int id) {
        String query = "delete from PricePackage where PackageId = ?";
        try {
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
            
    public static void main(String[] args) {
        PriceDAO u = new PriceDAO();
//        List<Price> list = u.getAll();
//        for (Price price : list) {
//            System.out.println(price.isStatus());
//        }

        u.editPrice("comboVVip", "99", true, Double.parseDouble("505"), Double.parseDouble("155"), "Forever", 7);
    }

}
