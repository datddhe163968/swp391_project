/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import model.Account;
import model.Blog;
import model.Price;
import model.Register;

/**
 *
 * @author DAT
 */
public class RegisterDAO extends MyDAO {

    private List<Register> list;

    public void clearData() {
        list.clear();
    }

    public List<Register> getSort(String sortOrder, int id) {
        List<Register> list = new ArrayList<>();
        try {
            // Kết nối với CSDL MySQL

            // Thực hiện truy vấn SQL để lấy danh sách các bài đăng
            String orderByClause = "";
            if (sortOrder != null) {
                switch (sortOrder) {
                    case "1":
                        orderByClause = "SELECT * FROM registration WHERE RegTime >= DATEADD(wk, DATEDIFF(wk, 0, GETDATE())-1, 0) AND RegTime < DATEADD(wk, DATEDIFF(wk, 0, GETDATE()), 0)";
                        break;
                    case "2":
                        orderByClause = "SELECT *  FROM registration WHERE RegTime >= DATEADD(month, DATEDIFF(month, 0, GETDATE())-1, 0) AND RegTime < DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)";
                        break;
                    case "3":
                        orderByClause = "SELECT * FROM registration WHERE RegTime >= DATEADD(month, DATEDIFF(month, 0, GETDATE())-3, 0) AND RegTime < DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)";
                        break;
                    case "4":
                        orderByClause = " SELECT * FROM registration WHERE RegTime >= DATEADD(month, DATEDIFF(month, 0, GETDATE())-6, 0) AND RegTime < DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)";
                        break;
                    case "0":
                        orderByClause = "SELECT * FROM registration WHERE RegTime <= DATEADD(day, -1, EOMONTH(GETDATE()))";
                        break;
                    default:
                        break;
                }
            }
            String sql = orderByClause + "And UserId =?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                int UserID = rs.getInt("UserId");
                String RegTime = rs.getString("RegTime");
                int PackageId = rs.getInt("PackageId");
                double Cost = rs.getDouble("Cost");
                String ValidFrom = rs.getString("ValidFrom");
                String ValidTo = rs.getString("ValidTo");
                boolean status = rs.getBoolean("status");
                int CourseId = rs.getInt("CourseId");

                // create object
                Register reg = new Register(UserID, RegTime, PackageId, Cost, ValidFrom, ValidTo, status, CourseId);

                list.add(reg);

            }

            // Đóng kết nối và giải phóng tài nguyên
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Register> Search(String txtSearch, int id) {
        List<Register> list = new ArrayList<>();
        xSql = "SELECT * FROM Registration WHERE  CourseId IN (SELECT CourseId FROM Course Where CourseName LIKE ? )And UserId = ?";

        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + txtSearch + "%");
            ps.setInt(2, id);

            rs = ps.executeQuery();
            while (rs.next()) {
                int UserID = rs.getInt("UserId");
                String RegTime = rs.getString("RegTime");
                int PackageId = rs.getInt("PackageId");
                double Cost = rs.getDouble("Cost");
                String ValidFrom = rs.getString("ValidFrom");
                String ValidTo = rs.getString("ValidTo");
                boolean status = rs.getBoolean("status");
                int CourseId = rs.getInt("CourseId");

                Register reg = new Register(UserID, RegTime, PackageId, Cost, ValidFrom, ValidTo, status, CourseId);

                list.add(reg);

            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Register> getCourse(int id) {
        List<Register> list = new ArrayList<>();
        xSql = "select CourseId from Registration where UserId = ? ";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                int CourseId = rs.getInt(1);
//                String RegTime = rs.getString(2);
//                   int     PackageId = rs.getInt(3);
//                    double    Cost = rs.getDouble(4);
//                    String    ValidFrom = rs.getString(5);
//                    String    ValidTo = rs.getString(6);
//               boolean status = rs.getBoolean(7);

                // create object
                //  Register reg = new Register(UserId, RegTime, PackageId, Cost, ValidFrom, ValidTo, status, UserId);
                Register reg = new Register(CourseId);
                list.add(reg);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int deleteRegister(int courseid, int userid) {
        int n = 0;
        xSql = "  DELETE FROM [Registration] WHERE [CourseId] = ? and [UserId] =? ;";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, courseid);
            ps.setInt(2, userid);
            n = ps.executeUpdate();

            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return n;

    }

    public List<Register> getRegister(int id) {
        List<Register> list = new ArrayList<>();
        xSql = "select * from Registration where UserId = ? ";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                int UserID = rs.getInt("UserId");
                String RegTime = rs.getString("RegTime");
                int PackageId = rs.getInt("PackageId");
                double Cost = rs.getDouble("Cost");
                String ValidFrom = rs.getString("ValidFrom");
                String ValidTo = rs.getString("ValidTo");
                boolean status = rs.getBoolean("status");
                int CourseId = rs.getInt("CourseId");

                Register reg = new Register(UserID, RegTime, PackageId, Cost, ValidFrom, ValidTo, status, CourseId);

                list.add(reg);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int Register(Register reg) {
        int n = 0;
        LocalDateTime curDate = java.time.LocalDateTime.now();
        String date = curDate.toString();
        xSql = "INSERT INTO [dbo].[Registration]\n"
                + "         \n"
                + "           ([UserId]\n"
                + "		   ,[RegTime]\n"
                + "		   ,[PackageId]\n"
                + "           ,[Cost]\n"
                + "           ,[ValidFrom]\n"
                + "           ,[ValidTo]\n"
                + "           ,[status]\n"
                + "           ,[CourseId]) "
                + "     VALUES (?,?,?,?,?,?,?,?)";
        try {
            String m = "1";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, reg.getUserId());
            ps.setString(2, date);
            ps.setInt(3, reg.getPackageId());
            ps.setDouble(4, reg.getCost());
            ps.setString(5, date);
            ps.setString(6, reg.getValidTo());
            ps.setString(7, m);
            ps.setInt(8, reg.getCourseId());
            n = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return n;
    }

    public int countRegisterByCourse(int CourseId) {
        String sql = "select COUNT (*) from registration where CourseId = ?";
        int s = 0;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, CourseId);
            rs = ps.executeQuery();
            if (rs.next()) {
                s = rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

//    public int countTime(Date date) {
//        String sql = "SELECT COUNT(*) AS total FROM Registration WHERE DAY(RegTime) = ? And MONTH(RegTime) = ? And YEAR(RegTime) = ?";
//        System.out.println(date.getDay());
//        System.out.println(date.getMonth());
//        System.out.println(date.getYear());
//        System.out.println(date);
//        ///////
//        
//        try {
//            ps = con.prepareStatement(sql);
//            ps.setInt(1, date.getDay());
//            ps.setInt(2, date.getMonth());
//            ps.setInt(3, date.getYear());
////            ps.setString(2, toDate);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                return rs.getInt("total");
//            }
//        } catch (Exception e) {
//
//        }
//        return 0;
//    }

    
    public int countTime(String startDate, String endDate) {
        String sql = "SELECT COUNT(*) AS total FROM Registration WHERE RegTime BETWEEN ? and ?";
        try {
            // Sử dụng lớp java.util.Calendar để truy xuất ngày, tháng và năm từ đối tượng Date
            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH) + 1; // Tháng bắt đầu từ 0, nên cần cộng 1
            int year = calendar.get(Calendar.YEAR);
            ps = con.prepareStatement(sql);
            ps.setDate(1, new java.sql.Date(calendar.getTime().getTime()));
            ps.setDate(2, new java.sql.Date(calendar.getTime().getTime()));
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }

//    public ArrayList<Integer> getOrderDataInPeriod(String startDate, String enddate) {
//        ArrayList<Integer> orderDatas = new ArrayList<>();
//        try {
//            String sql = "WITH DateRange(DateData) AS \n"
//                    + "(\n"
//                    + "    SELECT cast(? as date) as [Date]\n"
//                    + "    UNION ALL\n"
//                    + "    SELECT DATEADD(d,1,DateData)\n"
//                    + "    FROM DateRange \n"
//                    + "    WHERE DateData < cast(? as date)\n"
//                    + ")\n"
//                    + "SELECT dr.DateData as dateInPeriod,\n"
//                    + "COUNT(case when r.[status] is null then null else 1 end) as totalRegistration, \n"
//                    + "COUNT(case r.[status] when 'true' then 1 else null end) as successRegistration\n"
//                    + "FROM DateRange dr left join Registration r on cast(r.registrationTime as date) = dr.DateData\n"
//                    + "group by dr.DateData\n"
//                    + "OPTION (MAXRECURSION 0)";
//            PreparedStatement stm = connection.prepareStatement(sql);
//            stm.setString(1, startDate);
//            stm.setString(2, enddate);
//            ResultSet rs = stm.executeQuery();
//            while (rs.next()) {
//                OrderData orderData = new OrderData();
//                orderData.setDate(sdf.format(rs.getDate("dateInPeriod")));
//                orderData.setAll(rs.getInt("totalRegistration"));
//                orderData.setSuccess(rs.getInt("successRegistration"));
//                orderDatas.add(orderData);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(RegistrationDBContext.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return orderDatas;
//    }
    public int countRegister() {
        String sql = "select COUNT (*) from registration ";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public List<Register> countUserRegister() {
        List<Register> list = new ArrayList<>();
        String sql = "select top 3 username , count(totalprice) as 'counts' from account u\n"
                + "join orders o \n"
                + "on u.userID = o.userID\n"
                + "group by username\n"
                + "order by counts desc";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getString(1));
                list.add(new Register(a, rs.getInt(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public ArrayList<Register> countCustomerCourse() {
        ArrayList<Register> list = new ArrayList<>();
        xSql = "SELECT TOP 3 * FROM Registration ORDER BY RegTime DESC ";

        try {
            ps = con.prepareStatement(xSql);

            rs = ps.executeQuery();
            while (rs.next()) {
                int UserID = rs.getInt("UserId");
                String RegTime = rs.getString("RegTime");
                int PackageId = rs.getInt("PackageId");
                double Cost = rs.getDouble("Cost");
                String ValidFrom = rs.getString("ValidFrom");
                String ValidTo = rs.getString("ValidTo");
                boolean status = rs.getBoolean("status");
                int CourseId = rs.getInt("CourseId");

                Register reg = new Register(UserID, RegTime, PackageId, Cost, ValidFrom, ValidTo, status, CourseId);

                list.add(reg);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Register> selectAll() {
        ArrayList<Register> list = new ArrayList<>();
        xSql = "SELECT RegId, UserId, RegTime, PackageId, Cost, ValidFrom, ValidTo, CourseId FROM Registration";

        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Register x = new Register();
                // Lấy giá trị từ ResultSet và đặt vào các thuộc tính của đối tượng Register
                x.setRegId(rs.getInt("RegId"));
                x.setUserId(rs.getInt("UserId"));
                x.setRegTime(rs.getString("RegTime"));
                x.setPackageId(rs.getInt("PackageId"));
                x.setCost(rs.getDouble("Cost"));
                x.setValidFrom(rs.getString("ValidFrom"));
                x.setValidTo(rs.getString("ValidTo"));
                x.setCourseId(rs.getInt("CourseId"));
                list.add(x);
            }

            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

//    public ArrayList<Register> selectAll(){
//        ArrayList<Register> list = new ArrayList<>();
//        xSql = "select * from Registration";
//        try {
//            ps = con.prepareStatement(xSql);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                Register x = new Register();
//                list.add(x);
//            }
//            rs.close();
//            ps.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return (list);
//    }
    public List<Register> getRegistrationFormTo(Date from, Date to) {
        List<Register> list = new ArrayList<>();

        String sql = "SELECT RegId, UserId, RegTime, PackageId, ValidFrom, ValidTo "
                + "FROM Registration "
                + "WHERE RegTime BETWEEN ? AND ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDate(1, from);
            st.setDate(2, to);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Register re = new Register();
                re.setRegId(rs.getInt("RegId"));
                re.setUserId(rs.getInt("UserId"));
                re.setRegTime(rs.getString("RegTime"));
                re.setPackageId(rs.getInt("PackageId"));
                re.setValidFrom(rs.getString("ValidFrom"));
                re.setValidTo(rs.getString("ValidTo"));
                list.add(re);
            }

            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;

    }
    
        public List<Register> getListByPage(List<Register> list,
            int start, int end) {
        ArrayList<Register> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static void main(String[] args) {

//        List<Register> lst = r.countTime();
//        // tạo đối tượng Account và đối tượng Register
//        int n;
//        n = dao.Register(new Register(1, "2023-06-04 10:30:00", 2, 600, "2023-06-04 10:30:00", "2023-07-04 10:30:00", true,  6));
//        System.out.println(n);
//    //  List<Register> c = dao.getSort("1",4);
//    //    dao.deleteRegister(2, 4);
//      // System.out.println(c);
        // Tạo ngày bắt đầu và ngày kết thúc cho khoảng thời gian
//        RegisterDAO r = new RegisterDAO();
//        Date fromDate = Date.valueOf("2023-06-05");
//        Date toDate = Date.valueOf("2023-06-11");
////        int totalRegisteredCourses = r.countTime(date, date);
////        System.out.println("Tổng số khóa học đã đăng ký trong khoảng thời gian là: " + totalRegisteredCourses);
//        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy");
//        String date1 = "06/06/2023";
//        try {
//            java.util.Date date = s.parse(date1);
//            int count = r.countTime((Date) date);
//            System.out.println(count);
//        } catch (Exception e) {
//
//        }
        RegisterDAO rdao = new RegisterDAO();
        AccountDAO a = new AccountDAO();
//        Date fromDate = new Date();  // Ngày bắt đầu (sử dụng ngày hiện tại)
//        Date toDate = new Date();  // Ngày kết thúc (sử dụng ngày hiện tại)
        //List<Register> lst = rdao.getRegistrationFormTo(Date.valueOf("2023-06-01"), Date.valueOf("2023-06-13"));
        String n = null;
   n= rdao.getCourseName(4);
        System.out.println(n);
//        List<Register> list = rdao.selectAll();
//
//            System.out.println(list);
//        
//        int countTime = rdao.countTime("2023-06-01", "2023-06-30");
//        System.out.println(countTime);
//        Date date = new Date(); // Hoặc bạn có thể tạo đối tượng Date với ngày/tháng/năm cụ thể
//
//        // Tạo một đối tượng của lớp chứa hàm countTime
//
//        // Gọi hàm countTime và in kết quả ra màn hình
//        int result = rdao.countTime(date);
//        System.out.println("Total count: " + result);
    }
    public String getCourseName(int userid) {
        String s = null;
        String sql = "SELECT Registration.CourseId, Course.CourseName FROM Registration JOIN Course ON Course.CourseId = Registration.CourseId where Course.CourseId =?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                s = rs.getString("CourseName");
            }
        } catch (Exception e) {
        }
        return s;
    }
     public String getPrice(int userid) {
        String s = null;
        String sql = "SELECT Registration.PackageId, PricePackage.Description FROM Registration JOIN PricePackage ON PricePackage.PackageId = Registration.PackageId where	PricePackage.PackageId =? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                s = rs.getString("Description");
            }
        } catch (Exception e) {
        }
        return s;
    }
    
}
