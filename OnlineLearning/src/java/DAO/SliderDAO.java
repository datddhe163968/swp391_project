/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Slider;

/**
 *
 * @author dell
 */
public class SliderDAO extends MyDAO{
    public int countSlider() {
        int count = 0;
        String sql = "select count(*) as counts from slider";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return count;
    }
    
    public ArrayList<Slider> selectAll() {
        ArrayList<Slider> t = new ArrayList<>();
        xSql = "select * from Slider";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                String urlImage = rs.getString("urlImage");
                String CatId = rs.getString("Category");
                String backLink = rs.getString("BackLink");
                boolean status = rs.getBoolean("Status");
                String note = rs.getString("Note");
                String title = rs.getString("Title");
                Slider x = new Slider(id, urlImage, CatId, backLink, status, note,title);
                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (t);
    }

    public Slider selectByID(int id) {
        xSql = "select * from Slider where ID = ? ";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                String urlImage = rs.getString("urlImage");
                String Category = rs.getString("Category");
                String backLink = rs.getString("BackLink");
                boolean status = rs.getBoolean("Status");
                String note = rs.getString("Note");
                String title = rs.getString("Title");
                Slider x = new Slider(id, urlImage, Category, backLink, status, note,title);
                return new Slider(id, urlImage, Category, backLink, status, note,title);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    

    public void updateSlider(int id,String filename, String catname, String status,String backlink,String note, String title) {
        String xSql = "Update Slider set UrlImage=? , Category =?, Status=?, Note=?, Title = ?, BackLink = ? where ID =?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, filename);
            ps.setString(2, catname);
            ps.setString(3, status);
            ps.setString(4, note);
            ps.setString(5, title);
            ps.setString(6, backlink);
            ps.setInt(7, id);
            
            ps.executeUpdate();
        } catch (SQLException e) {
        }
    }
    public int getMaxId() {
    int id = 0;
    String sql = "SELECT MAX(ID) AS max_id FROM Slider";
    try {
        ps = con.prepareStatement(sql);
        rs = ps.executeQuery();
        if (rs.next()) {
            id = rs.getInt("max_id");
        }
    } catch (SQLException e) {
        
    }
    return id;
}


    public void InsertSlider(String category, String image,String status,String title,String backlink,String note) {
        SliderDAO u = new SliderDAO();
        int id = u.getMaxId()+1;
        try {
            String sql = "insert into dbo.[Slider] \n"
                    + "(Category,UrlImage,Status,Title,Note,ID,BackLink) \n"
                    + "values (?,?,?,?,?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, category);
            ps.setString(2, image);
            ps.setString(3, status);
            ps.setString(4, title);
            ps.setString(5, note);
            ps.setInt(6, id);
            ps.setString(7, backlink);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteSlider(int id) {
        String query = "delete from slider where ID = ?";
        try {
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    public ArrayList<Slider> SearchByName(String str) {
        ArrayList<Slider> t = new ArrayList<>();
        xSql = "select * from Slider where Title like ? ";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + str + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                String urlImage = rs.getString("urlImage");
                String CatId = rs.getString("Category");
                String backLink = rs.getString("BackLink");
                boolean status = rs.getBoolean("Status");
                String note = rs.getString("Note");
                String title = rs.getString("Title");
                Slider x = new Slider(id, urlImage, CatId, backLink, status, note, title);
                t.add(x);

            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (t);
    }
    public List<Slider> getListByPage(List<Slider> list,int start, int end) {
        ArrayList<Slider> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
    public ArrayList<Slider> FilterByStatus(boolean status){
        ArrayList<Slider> t = new ArrayList<>();
        xSql = "select * from Slider where status = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1,status);
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                String urlImage = rs.getString("urlImage");
                String CatId = rs.getString("Category");
                String backLink = rs.getString("BackLink");
                String note = rs.getString("Note");
                String title = rs.getString("Title");
                Slider x = new Slider(id, urlImage, CatId, backLink, status, note, title);
                t.add(x);

            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (t);
    }
    public static void main(String[] args) {
       SliderDAO u = new SliderDAO();
       u.updateSlider(11, "b", "b", "false", "b", "b", "b");
    }
}
