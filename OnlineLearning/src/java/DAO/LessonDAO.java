/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import model.CourseLesson;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import model.LessonType;
import model.Register;

/**
 *
 * @author admin
 */
public class LessonDAO extends MyDAO {

    public List<CourseLesson> getByCourseId(int CourseId) {
        xSql = "select * from  Lesson where CourseId = ' " + CourseId + "'";
        List<CourseLesson> t = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int xCourseId = CourseId;
            int xLessonId;
            String xLessonName;
            String xLessonTopic;
            int xLessonOrder;
            int xLessonTypeId;
            String xVideoLink;
            String xContent;
            boolean xstatus;
            int xQuizId;
            CourseLesson x;
            while (rs.next()) {
                xCourseId = rs.getInt("CourseId");
                xLessonId = rs.getInt("LessonId");
                xLessonName = rs.getString("LessonName");
                xLessonTopic = rs.getString("LessonTopic");
                xLessonOrder = rs.getInt("LessonOrder");
                xLessonTypeId = rs.getInt("LessonTypeId");
                xVideoLink = rs.getString("VideoLink");
                xContent = rs.getString("Content");
                xstatus = rs.getBoolean("status");
                xQuizId = rs.getInt("QuizId");
                x = new CourseLesson(xLessonId, xCourseId, xLessonName, xLessonTopic, xLessonOrder, xLessonTypeId, xVideoLink, xContent, xstatus, xQuizId);

                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;

    }

    public int CountLesson(int CourseId) {
        xSql = "SELECT COUNT(LessonId) FROM Lesson where CourseId = ?";
        int count = 0;
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, CourseId);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public String GetCourseName(int id) {
        switch (id) {
            case 1:
                return "Math";
            case 2:
                return "Chemistry";
            case 3:
                return "Physics";
            case 4:
                return "Music";
            case 5:
                return "Biology";
            case 6:
                return "Literature";
            case 7:
                return "English";
            case 8:
                return "Science";
            case 9:
                return "Geography";
            case 10:
                return "History";
        }
        return null;
    }

    public String getCourseImg(int id) {
        xSql = "select image from  Course where CourseId = ?";
        String s = null;
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                s = rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    public static void main(String[] args) {
        LessonDAO u = new LessonDAO();
        //  List<CourseLesson> name = u.getLessonbystatus( "1","0");
        //System.out.println(name);
        //  u.updateLesson("Dao Ham", "Algebra", "https://www.youtube.com/embed/w7CUCeWzOo", "Đạo hàm là một khái niệm trong toán học. Đạo hàm của một hàm số là độ dốc của đường cong của hàm số tại một điểm nhất định. Nó được định nghĩa là giới hạn của tỷ số đạo độ của hàm số và đạo độ của biến độc lập, khi đạo độ của biến độc lập tiến gần đến 0.", "1");

        int n;
   n= u.AddLesson(new CourseLesson(39,1, "Dao", "tutu",1 , "video", "huhu", true));
        //n = u.getLatestLessonId();
        //    System.out.println(n);
    }

    public void updateStatus(String st, String Id) {

        try {
            String orderByClause = "";

            xSql = "UPDATE Lesson SET status = ? WHERE LessonId = ?";

            ps = con.prepareStatement(xSql);
            ps.setString(1, st);

            ps.setString(2, Id);

            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<CourseLesson> getLessonbystatus(int Course, String st) {
        xSql = "select * from Lesson where status = ? and CourseId = ?";
        List<CourseLesson> t = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, st);
            ps.setInt(2, Course);
            rs = ps.executeQuery();

            int xCourseId;
            int xLessonId;
            String xLessonName;
            String xLessonTopic;
            int xLessonOrder;
            int xLessonTypeId;
            String xVideoLink;
            String xContent;
            boolean xstatus;
            int xQuizId;
            CourseLesson x;
            while (rs.next()) {
                xCourseId = rs.getInt("CourseId");
                xLessonId = rs.getInt("LessonId");
                xLessonName = rs.getString("LessonName");
                xLessonTopic = rs.getString("LessonTopic");
                xLessonOrder = rs.getInt("LessonOrder");
                xLessonTypeId = rs.getInt("LessonTypeId");
                xVideoLink = rs.getString("VideoLink");
                xContent = rs.getString("Content");
                xstatus = rs.getBoolean("status");
                xQuizId = rs.getInt("QuizId");
                x = new CourseLesson(xLessonId, xCourseId, xLessonName, xLessonTopic, xLessonOrder, xLessonTypeId, xVideoLink, xContent, xstatus, xQuizId);

                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;

    }

    public List<CourseLesson> search(String txt, int id) {
        xSql = "select * from Lesson  where LessonName like ? and CourseId =?";
        List<CourseLesson> t = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + txt + "%");
            ps.setInt(2, id);
            rs = ps.executeQuery();

            int xCourseId;
            int xLessonId;
            String xLessonName;
            String xLessonTopic;
            int xLessonOrder;
            int xLessonTypeId;
            String xVideoLink;
            String xContent;
            boolean xstatus;
            int xQuizId;
            CourseLesson x;
            while (rs.next()) {
                xCourseId = rs.getInt("CourseId");
                xLessonId = rs.getInt("LessonId");
                xLessonName = rs.getString("LessonName");
                xLessonTopic = rs.getString("LessonTopic");
                xLessonOrder = rs.getInt("LessonOrder");
                xLessonTypeId = rs.getInt("LessonTypeId");
                xVideoLink = rs.getString("VideoLink");
                xContent = rs.getString("Content");
                xstatus = rs.getBoolean("status");
                xQuizId = rs.getInt("QuizId");
                x = new CourseLesson(xLessonId, xCourseId, xLessonName, xLessonTopic, xLessonOrder, xLessonTypeId, xVideoLink, xContent, xstatus, xQuizId);

                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;

    }

    public List<CourseLesson> getListByPage(List<CourseLesson> list, int start, int end) {
        ArrayList<CourseLesson> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public CourseLesson LessonByID(String id) {
        xSql = "select * from Lesson where LessonId = ?";
        List<CourseLesson> t = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            rs = ps.executeQuery();

            int xCourseId;
            int xLessonId;
            String xLessonName;
            String xLessonTopic;
            int xLessonOrder;
            int xLessonTypeId;
            String xVideoLink;
            String xContent;
            boolean xstatus;
            int xQuizId;
            CourseLesson x;
            while (rs.next()) {
                xCourseId = rs.getInt("CourseId");
                xLessonId = rs.getInt("LessonId");
                xLessonName = rs.getString("LessonName");
                xLessonTopic = rs.getString("LessonTopic");
                xLessonOrder = rs.getInt("LessonOrder");
                xLessonTypeId = rs.getInt("LessonTypeId");
                xVideoLink = rs.getString("VideoLink");
                xContent = rs.getString("Content");
                xstatus = rs.getBoolean("status");
                xQuizId = rs.getInt("QuizId");
                x = new CourseLesson(xLessonId, xCourseId, xLessonName, xLessonTopic, xLessonOrder, xLessonTypeId, xVideoLink, xContent, xstatus, xQuizId);

                return x;
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<LessonType> LessonType() {
        xSql = "select * from LessonType";
        List<LessonType> t = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);

            rs = ps.executeQuery();

            int xLessonTypeId;
            String xLessonTypeName;
            LessonType x;
            while (rs.next()) {
                xLessonTypeId = rs.getInt("LessonTypeId");
                xLessonTypeName = rs.getString("LessonTypeName");

                x = new LessonType(xLessonTypeId, xLessonTypeName);

                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;

    }

    public void updateLesson(String name, String topic, String link, String content, String Id) {

        try {
            String orderByClause = "";

            xSql = "UPDATE Lesson SET LessonName = ?,LessonTopic = ?,VideoLink = ?,Content =? WHERE LessonId = ?";

            ps = con.prepareStatement(xSql);
            ps.setString(1, name);
            ps.setString(2, topic);
            ps.setString(3, link);
            ps.setString(4, content);

            ps.setString(5, Id);

            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public int AddLesson(CourseLesson reg) {
        int n = 0;

        xSql = "INSERT INTO [dbo].[Lesson]\n"
                + "           ([LessonId]\n"
                + "           ,[CourseId]\n"
                + "           ,[LessonName]\n"
                + "           ,[LessonTopic]\n"
                + "           ,[LessonTypeId]\n"
                + "           ,[VideoLink]\n"
                + "           ,[Content]\n"
                + "           ,[Status])\n"
                + "     VALUES (?,?,?,?,?,?,?,?)";
        try {
            String m = "1";
            ps = con.prepareStatement(xSql);
            ps.setInt(1, reg.getLessonId());
            ps.setInt(2, reg.getCourseId());
            ps.setString(3, reg.getLessonName());
            ps.setString(4, reg.getLessonTopic());
            ps.setInt(5, reg.getLessonTypeId());
            ps.setString(6, reg.getVideoLink());
            ps.setString(7, reg.getContent());
            ps.setBoolean(8, reg.isStatus());
            n = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return n;
    }

    public int getLatestLessonId() {
        int latestLessonId = -1;
        xSql = "SELECT TOP 1 LessonId FROM [dbo].[Lesson] ORDER BY LessonId DESC";
        try {
            ps = con.prepareStatement(xSql);

            rs = ps.executeQuery();
            if (rs.next()) {
                latestLessonId = rs.getInt("LessonId");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return latestLessonId;
    }

    public List<String> getChapterFromLessonList(List<CourseLesson> lessons) {
        List<Integer> check = new ArrayList();
        List<String> chapters = new ArrayList();
        for (CourseLesson lesson : lessons) {
            if (!check.contains(lesson.getLessonOrder())) {
                int chapter = lesson.getLessonOrder();
                String lessonTopic = lesson.getLessonTopic();
                check.add(chapter);
                chapters.add("Chapter " + chapter + ": " + lessonTopic);
            }
        }
        return chapters;
    }
}
