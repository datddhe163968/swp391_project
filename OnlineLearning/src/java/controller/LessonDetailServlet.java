package controller;

import DAO.LessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import model.CourseLesson;
import model.LessonType;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import jakarta.servlet.http.Part;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import jakarta.servlet.annotation.MultipartConfig;

/**
 *
 * @author DAT
 */
@WebServlet(name = "LessonDetailServlet", urlPatterns = {"/lessondetail"})
@MultipartConfig(maxFileSize = 1048576, maxRequestSize = 2097152)
public class LessonDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String id = request.getParameter("bid");
                        int course = Integer.parseInt(request.getParameter("course"));

           
            
            LessonDAO dao = new LessonDAO();
            
           List<LessonType> lst = dao.LessonType();
            CourseLesson c = dao.LessonByID(id);
             String videolink = request.getParameter("videolink");
             String Type = request.getParameter("Type");
            String lessonname = request.getParameter("lessonname");
            String lessontopic = request.getParameter("lessontopic");
            String lessonid = request.getParameter("id");
            String model = request.getParameter("model");
            String lessoncontent = request.getParameter("lessoncontent");
                        String referer = request.getParameter("referer");
                        String referer1 = request.getHeader("Referer");
                      
String filename = null;
LocalDateTime curDate = java.time.LocalDateTime.now();
        String date = curDate.toString();
            System.out.println("cc" +videolink);
            try {
                Part html = request.getPart("html");
               
                String realPath = "F:\\Summer23\\SWP391\\gitt\\summer2023-swp391.se1714-g3\\OnlineLearning\\web\\assets\\html";
                String submittedFileName = curDate.toString().replace(":", "") + "_" + html.getSubmittedFileName();

                filename = Paths.get(submittedFileName).getFileName().toString();

                if (!Files.exists(Paths.get(realPath))) {
                    Files.createDirectory(Paths.get(realPath));
                }
                html.write(realPath + "/" + filename);
                filename = "assets/html/" + filename;
                

            } catch (Exception e) {
            }
              if (filename != null&& filename.endsWith("html")) {
               dao.updateLesson(lessonname, lessontopic, filename, lessoncontent, lessonid);
               c = dao.LessonByID(id);
               
             request.setAttribute("lessondetail", c);
            request.setAttribute("lst", lst);
            request.setAttribute("id", id);
            request.setAttribute("course", course);
                System.out.println("ax"+referer1);  
          response.sendRedirect("sublist");
            }
            if(lessonname != null && videolink!= null && videolink.startsWith("https://www.youtube.com/embed/") ){
                
                dao.updateLesson(lessonname, lessontopic, videolink, lessoncontent, lessonid);
            c = dao.LessonByID(id);
            String mess = "Update thành công";
            request.setAttribute("successMessage", mess);
             request.setAttribute("lessondetail", c);
            request.setAttribute("lst", lst);
            request.setAttribute("id", id);
            request.setAttribute("course", course);
   
           response.sendRedirect(referer1);
            }if(model!= null){
            
            request.setAttribute("lessondetail", c);
            request.setAttribute("Type", Type);
            request.setAttribute("lst", lst);
            request.setAttribute("id", id);
            request.setAttribute("course", course);
            request.getRequestDispatcher("LessonDetail.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

