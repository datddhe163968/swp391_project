package controller;

import DAO.DAOBlog;
import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import model.Blog;
import model.Blog_Catergory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author admin
 */
@MultipartConfig(maxFileSize = 1048576, maxRequestSize = 2097152)
public class EditPostServlet extends HttpServlet {

    /**
     * URL pattern : /editpost
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        DAOBlog dao = new DAOBlog();
        String anh = request.getParameter("anh");
        String flag = request.getParameter("flag");
        String filename;
        try{
        //Upload photo
        Part part = request.getPart("image");
        String realPath = "C:\\Users\\admin\\Desktop\\SWP391\\summer2023-swp391.se1714-g3\\OnlineLearning\\web\\image";
//        String realPath = request.getServletContext().getRealPath("/image/");
        String submittedFileName = part.getSubmittedFileName();
        filename = Paths.get(submittedFileName).getFileName().toString();
        if (!Files.exists(Paths.get(realPath))) {
            Files.createDirectory(Paths.get(realPath));
        }
        part.write(realPath + "/" + filename);
        filename = "image/" + filename;
//        out.println(filename);
        }catch(Exception e){
            filename = anh;
            
        }
        String bid = request.getParameter("bid");
        int BlogId = Integer.parseInt(bid);
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String updatedate = request.getParameter("update-date");
        
        String category = request.getParameter("category");
        String blogdetail = request.getParameter("blog-detail");
//        out.println(title);
//        out.println(author);
//        out.println(updatedate);
//        out.println(createdate);
//        out.println(category);
//        out.println(blogdetail);
//        out.print(filename);
        dao.EditInfor(BlogId,filename,title,blogdetail,author,updatedate,category);
        dao.EditFlag(flag,BlogId);
        List<Blog_Catergory> list1 = dao.getAllCa();
        Blog last = dao.getLast();
        Blog b = dao.getByID(bid);
        Blog_Catergory c = dao.getCaID(bid);
        List<Blog> list = dao.getAll();
        List<Blog> FlagBlog = dao.getFlagBlog();
        String sortOrder = request.getParameter("sortOrder"); // Lấy tham số sortOrder từ request
        List<Blog> listS = dao.getSort(sortOrder);
        
        int page;
            int numberpage = 6;
            int size = list.size();
            int num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);
            String xpage = request.getParameter("page");
            if (xpage == null) {
                page = 1;
            } else if (!xpage.matches("[0-9]+")) {
                response.sendRedirect("error.jsp");
                return;

            } else {
                page = Integer.parseInt(xpage);
            }
            int start, end;
            start = (page - 1) * numberpage;
            end = Math.min(page * numberpage, size);
            List<Blog> listP = dao.getListByPage(list, start, end);
            List<Blog> listPS = dao.getListByPage(listS, start, end);
            request.setAttribute("listP", listP);
            request.setAttribute("listP", listPS);
            request.setAttribute("list1", list1);
            request.setAttribute("last", last);
            request.setAttribute("num", num);
            request.setAttribute("page", page);
            request.setAttribute("FlagBlog", FlagBlog);
        
        request.getRequestDispatcher("MktBlogList.jsp").forward(request, response);
    }
    
}
        
