/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.AccountDAO;
import DAO.RegisterDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Account;
import model.Register;

/**
 *
 * @author DAT
 */
@WebServlet(name = "filterRegistration", urlPatterns = {"/filterRegistration"})
public class FillterRegistation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            AccountDAO dao1 = new AccountDAO();

            String dateFilter = request.getParameter("dateFilter");
            RegisterDAO dao = new RegisterDAO();
            int userid = Integer.parseInt(request.getParameter("user-id"));
            List<Account> cc = dao1.getAccount(userid);

                     System.out.println("ae1" + dateFilter);
                     System.out.println("ae2" + userid);
          
            List<Register> listS = dao.getSort(dateFilter,userid);
            int page;
        int numberpage = 6;
        int size = listS.size();
        int num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);
        String xpage = request.getParameter("page");
        if (xpage == null) {
                    page = 1;
                } else if (!xpage.matches("[0-9]+")) {
                    response.sendRedirect("error.jsp");
                    return;

                } else {
                    page = Integer.parseInt(xpage);
                }
        int start, end;
        start = (page - 1) * numberpage;
        end = Math.min(page * numberpage, size);
        List<Register> listP = dao.getListByPage(listS, start, end);
        request.setAttribute("num", num);
        request.setAttribute("page", page);
        request.setAttribute("dateFilter", dateFilter);
   request.setAttribute("listRe", listP);
             

           
   if(!listS.isEmpty()){
        request.setAttribute("listcc", cc);
  
   }

            System.out.println("ssss" + listS);
            if (!listS.isEmpty()) {
                request.setAttribute("listcc", cc);

            }

            request.getRequestDispatcher("MyRegistrations.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
