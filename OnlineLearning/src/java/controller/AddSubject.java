
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.CourseCatergoryDAO;
import DAO.CourseDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Course;
import model.CourseCatergory;

/**
 *
 * @author dell
 */
@WebServlet(name = "AddSubject", urlPatterns = {"/addsubject"})
@MultipartConfig(maxFileSize = 1048576, maxRequestSize = 2097152)
public class AddSubject extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CourseDAO dao = new CourseDAO();
        List<Course> coursestatus = dao.getCourses();     
        CourseCatergoryDAO catergory_dao = new CourseCatergoryDAO();
        List<CourseCatergory> listCatergory = catergory_dao.selectAll();     
        request.setAttribute("coursestatus", coursestatus);
        request.setAttribute("listCatergory", listCatergory);
        request.getRequestDispatcher("AddNewSubject.jsp").forward(request, response);       
        processRequest(request, response);
        HttpSession session = request.getSession();
            
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        HttpSession session = request.getSession();
        String name = request.getParameter("content");
        String description = request.getParameter("Description");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDateTime curDate = java.time.LocalDateTime.now();
        String date = curDate.toString();
        Calendar calendar = Calendar.getInstance();
        double price = Double.parseDouble(request.getParameter("price"));
        boolean status = Integer.parseInt(request.getParameter("status")) != 0;
        String filename = null;
        try{
        Part part = request.getPart("avatar");
        String realPath = "C:\\Users\\dell\\Documents\\NetBeansProjects\\tuanVM\\summer2023-swp391.se1714-g3\\OnlineLearning\\web\\image";
        String submittedFileName = part.getSubmittedFileName();
        filename = Paths.get(submittedFileName).getFileName().toString();
        if (!Files.exists(Paths.get(realPath))) {
            Files.createDirectory(Paths.get(realPath));
        }
        part.write(realPath + "/" + filename);
        filename = "image/" + filename;
        }catch(Exception e){                   
        }
        int catergoryId = Integer.parseInt(request.getParameter("cat"));     
        Account a = (Account) session.getAttribute("user");
        int userId = a.getId();
        String featureflag = request.getParameter("featureflag") ;
        CourseDAO DAO = new CourseDAO();
        DAO.InsertCourse(name, description, date, price, status, filename, catergoryId, userId, featureflag);
        
		request.getSession().setAttribute("addSuccess", true);
		response.sendRedirect("sublist");
		request.getRequestDispatcher("AddNewSubject.jsp").forward(request, response);    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
