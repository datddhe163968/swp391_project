/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.PriceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Price;

/**
 *
 * @author dell
 */
@WebServlet(name = "EditPrice", urlPatterns = {"/editprice"})
public class EditPrice extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PriceDAO DAO = new PriceDAO();
            
        List<Price> pricestatus = DAO.getAll();      
        request.setAttribute("pricestatus", pricestatus);
        
        String PackageID_String = request.getParameter("Id");
        int PackageId = Integer.parseInt(PackageID_String);
        Price a = new Price();
        a = DAO.selectById(PackageId);
      
//        PriceDAO dao = new PriceDAO();
//        Price p = dao.selectById(a);
//
        request.setAttribute("price", a);

        request.getRequestDispatcher("EditPrice.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("user");
        int Id = a.getId();
        if (a == null) {
            request.getRequestDispatcher("login").forward(request, response);
        } else {
            String PackageID_String = request.getParameter("Id");
            int PackageId = Integer.parseInt(PackageID_String);

            String pname = request.getParameter("pname");
            String pduration = request.getParameter("pduration");
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            double plistprice = Double.parseDouble(request.getParameter("plistprice"));
            double psaleprice = Double.parseDouble(request.getParameter("psaleprice"));
            String pdescription = request.getParameter("pdescription");

            PriceDAO dao = new PriceDAO();
            dao.editPrice(pname, pduration, status, plistprice, psaleprice, pdescription, PackageId);
            request.getRequestDispatcher("listprice").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
