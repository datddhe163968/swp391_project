/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.AccountDAO;
import DAO.CourseDAO;
import DAO.CourseRegisterDAO;
import DAO.RegisterDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Blog;
import model.Course;
import model.CourseRegister;
import model.Price;
import model.Register;

/**
 *
 * @author dell
 */
@WebServlet(name = "AdminDashboardServlet", urlPatterns = {"/admindashboard"})
public class AdminDashboardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("user");
        if (a == null) {
            request.getRequestDispatcher("forbiddenAlert.jsp").forward(request, response);
        } else {
            if (a.getRoleId() != 3) {
                response.sendRedirect("forbiddenAlert.jsp");
            }
//        int page;
//        int numberpage = 6;
//        int size = lst.size();
//        int num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);
//        String xpage = request.getParameter("page");
//        if (xpage == null) {
//            page = 1;
//        } else {
//            page = Integer.parseInt(xpage);
//        }
//        int start, end;
//        start = (page - 1) * numberpage;
//        end = Math.min(page * numberpage, size);

//        String from = request.getParameter("dateFrom");
//        String to = request.getParameter("dateTo");
//
//        List<Register> list = rdao.getRegistrationFormTo(Date.valueOf(from), Date.valueOf(to));
//        request.setAttribute("from", from);
//        request.setAttribute("to", to);
            RegisterDAO r = new RegisterDAO();
            
            
            List<Register> listCustomer = r.countCustomerCourse();
            request.setAttribute("listCustomer", listCustomer);
            
            List<Register> listregistraion = r.selectAll();
            for (Register register : listregistraion) {
                System.out.println();
            }
            String from = request.getParameter("dateFrom");
            String to = request.getParameter("dateTo");
            if (from != null && to != null) {
                listregistraion = r.getRegistrationFormTo(Date.valueOf(from), Date.valueOf(to));
                int page, numperpage = 6;
                int size = listregistraion.size();
                int num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);//so trang
                String xpage = request.getParameter("page");
                if (xpage == null) {
                    page = 1;
                } else if (!xpage.matches("[0-9]+")) {
                    response.sendRedirect("forbiddenAlert.jsp");
                    return;
                } else {
                    page = Integer.parseInt(xpage);
                }
                if (page > 10) {
                    response.sendRedirect("forbiddenAlert.jsp");
                    return;
                }
                int start, end;
                start = (page - 1) * numperpage;
                end = Math.min(page * numperpage, size);
                List<Register> register = r.getListByPage(listregistraion, start, end);

                request.setAttribute("page", page);
                request.setAttribute("num", num);
                request.setAttribute("check", "list");
                request.setAttribute("from", Date.valueOf(from));
                request.setAttribute("to", Date.valueOf(to));
                request.setAttribute("listregistraion", register);
            }

            request.getRequestDispatcher("admindashboard.jsp").forward(request, response);
//        
//        LocalDate now1 = LocalDate.now();
//        LocalDate before = now1.plusDays(-7);
//
//        List<Register> previousList = rdao.getRegistrationFormTo(Date.valueOf(before), Date.valueOf(now1));
//        request.setAttribute("previousList", previousList);
//
//        request.setAttribute("list", list);

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
