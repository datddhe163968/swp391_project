/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.SliderDAO;
import model.Slider;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class SliderList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SliderListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SliderListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String SlideSearch = request.getParameter("SlideSearch");
        String xpage = request.getParameter("page");
        if(SlideSearch!=null){
            request.setAttribute("SlideSearch", SlideSearch);
            request.setAttribute("page", xpage);
            doPost(request, response);
        }else{
        SliderDAO dao = new SliderDAO();
        ArrayList<Slider> s = dao.selectAll();
//        request.setAttribute("listS", s);
        int page, numperpage = 4;
        int size = s.size();
        int num = (size % 4 == 0 ? (size / 4) : ((size / 4)) + 1);//so trang
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Slider> slider = dao.getListByPage(s, start, end);
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.setAttribute("check", "list");
        request.setAttribute("listS", slider);
        request.setAttribute("SlideSearch", SlideSearch);
        request.getRequestDispatcher("SliderList.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String slide = request.getParameter("SlideSearch");
        SliderDAO dao = new SliderDAO();
        ArrayList<Slider> s1 = dao.SearchByName(slide);
//        out.print(s1);
        int page, numperpage = 4;
        int size = s1.size();
        int num = (size % 4 == 0 ? (size / 4) : ((size / 4)) + 1);//so trang
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Slider> slider = dao.getListByPage(s1, start, end);
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.setAttribute("check", "search");
        request.setAttribute("listS", slider);
        request.setAttribute("SlideSearch", slide);
        request.getRequestDispatcher("SliderList.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
