package controller;

import DAO.AccountDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.*;

public class ChangePasswordServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        // Lấy thông tin từ request
        String newPassword = request.getParameter("new-password");
        Account user = (Account) request.getSession().getAttribute("user");
        if (user == null) {
            response.sendRedirect("forbiddenAlert.jsp");
        } else {
            AccountDAO aD = new AccountDAO();
            aD.updatePassword(user.getUserName(), newPassword);
            request.setAttribute("successMessage", "Change password successfully! Try login with new password");
            request.getRequestDispatcher("login.jsp").forward(request, response);

        }

    }
}
