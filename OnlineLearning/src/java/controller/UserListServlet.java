/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.AccountDAO;
import DAO.RoleDAO;
import DAO.UserStatusDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Role;
import model.UserStatus;

/**
 *
 * @author dell
 */
@WebServlet(name="UserListServlet", urlPatterns={"/userlist"})
public class UserListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDAO a = new AccountDAO();
        RoleDAO r = new RoleDAO();
        UserStatusDAO us = new UserStatusDAO();
 
        String action = request.getParameter("action");
        if (action == null){
            List<Account> account = a.selectAll();
            List<UserStatus> userstatus = us.selectAll();
            List<Role> role = r.selectAll();
            request.setAttribute("role", role);
            request.setAttribute("userstatus", userstatus);
            
            int page, numperpage = 8;
            int size = account.size();
            int num = (size % 8 == 0 ? (size / 8) : ((size / 8)) + 1);//so trang
            String xpage = request.getParameter("page");
            if (xpage == null) {
                page = 1;
            } else if (!xpage.matches("[0-9]+")) {
                response.sendRedirect("forbiddenAlert.jsp");
                return;
            } else {
                page = Integer.parseInt(xpage);
            }
            if (page > 8) {
                response.sendRedirect("forbiddenAlert.jsp");
                return;
            }
            int start, end;
            start = (page - 1) * numperpage;
            end = Math.min(page * numperpage, size);
            List<Account> userList = a.getListByPage(account, start, end);
            request.setAttribute("page", page);
            request.setAttribute("num", num);
            request.setAttribute("check", "list");
            request.setAttribute("account", userList);
            request.getRequestDispatcher("userlist.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}