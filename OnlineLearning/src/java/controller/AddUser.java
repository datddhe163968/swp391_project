/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.AccountDAO;
import Validate.Validate;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import model.Account;

/**
 *
 * @author dell
 */
@WebServlet(name = "AddUser", urlPatterns = {"/adduser"})
public class AddUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddUser</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddUser at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");        

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AccountDAO a = new AccountDAO();
        Validate validate  = new Validate();

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String fullname = request.getParameter("fullname");
        boolean gender = Boolean.parseBoolean(request.getParameter("gender"));
        String address = request.getParameter("address");
        String email = request.getParameter("email");
        String phonenum = request.getParameter("phonenum");
        String roleIdParam = request.getParameter("roleId");
//        boolean validateEmail = va.validateEmail(email);
//        boolean validatePhone = va.checkPhone(phonenum);
//        boolean validatePassword = va.checkPassword(password);
        String alertName = "", alertUserName = "", alertPassword = "", alertEmail = "", alertPhone = "";
        if (a.getUserByUserName(username) != null) {
            alertUserName = "Username already exist!";
            request.setAttribute("alertUserName", alertUserName);
            request.getRequestDispatcher("AddUser.jsp").forward(request, response);
            return;
        }
        if (!validate.validatePassword(password)) {
            alertPassword = "Passwords must have at least 6 characters!";
            request.setAttribute("alertPassword", alertPassword);
            request.getRequestDispatcher("AddUser.jsp").forward(request, response);
            return;
        }
        if (!validate.validateEmail(email)) {
            alertEmail = "Email is invalid!";
            request.setAttribute("alertEmail", alertEmail);
            request.getRequestDispatcher("AddUser.jsp").forward(request, response);
            return;
        }else if (a.getUserByEmail(email) != null) {
            alertEmail = "Email already exist!";
            request.setAttribute("alertEmail", alertEmail);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
        if (!validate.checkPhone(phonenum)) {
            alertPhone = "Phone number is invalid! Phone has to start with 0..";
            request.setAttribute("alertPhone", alertPhone);
            request.getRequestDispatcher("AddUser.jsp").forward(request, response);
            return;
        } else if (a.getUserByPhone(phonenum) != null) {
            alertPhone = "Phone already exist!";
            request.setAttribute("alertPhone", alertPhone);
            request.getRequestDispatcher("AddUser.jsp").forward(request, response);
            return;
        }
//        if (a1 != null) {
//            request.setAttribute("message", "Username is already exist");
//            request.getRequestDispatcher("AddUser.jsp").forward(request, response);
//        }
        int roleId = 0; // Giá trị mặc định nếu không thể chuyển đổi

        if (roleIdParam != null && !roleIdParam.isEmpty()) {
            roleId = Integer.parseInt(roleIdParam);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDateTime curDate = java.time.LocalDateTime.now();
        String date = curDate.toString();
        Calendar calendar = Calendar.getInstance();
        String statusIdParam = request.getParameter("statusId");
        int statusId = 0; // Giá trị mặc định nếu không thể chuyển đổi

        if (statusIdParam != null && !statusIdParam.isEmpty()) {
            statusId = Integer.parseInt(statusIdParam);
        }
        
        a.InsertAccount(username, password, fullname, gender, address, email, phonenum, roleId, date, statusId);
        request.getSession().setAttribute("addUserSuccess", true);

        request.getRequestDispatcher("userlist").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
