/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.LessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import model.CourseLesson;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author DAT
 */
@WebServlet(name = "AddLesson", urlPatterns = {"/addlesson"})
@MultipartConfig(maxFileSize = 1048576, maxRequestSize = 2097152)
public class AddLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            int courseid = Integer.parseInt(request.getParameter("courseid"));
            LessonDAO u = new LessonDAO();
            int n;
            n = u.getLatestLessonId();
            int m = n + 1;
            String videolink = request.getParameter("videolink");
//            String link = request.getParameter("link");
//            String referer1 = request.getHeader("Referer");
            String status = request.getParameter("status");
            String lessonname = request.getParameter("lessonname");
            String lessontopic = request.getParameter("lessontopic");
            String lessonid = request.getParameter("id");
            String lessoncontent = request.getParameter("lessoncontent");
             System.out.println("cúc cu2" + videolink);
            String filename = null;
            LocalDateTime curDate = java.time.LocalDateTime.now();
        String date = curDate.toString();
            try {
                Part html = request.getPart("html");
               
                String realPath = "C:\\Users\\admin\\Desktop\\SWP391\\summer2023-swp391.se1714-g3\\OnlineLearning\\web\\assets\\html";
                //String submittedFileName = html.getSubmittedFileName();
                String submittedFileName = curDate.toString().replace(":", "") + "_" + html.getSubmittedFileName();
                filename = Paths.get(submittedFileName).getFileName().toString();

                if (!Files.exists(Paths.get(realPath))) {
                    Files.createDirectory(Paths.get(realPath));
                }
                html.write(realPath + "/" + filename);
                filename = "assets/html/" + filename;
                System.out.println("cúc cu" + filename);

            } catch (Exception e) {
            }

            if (filename != null&& filename.endsWith("html")) {
                int typeID = Integer.parseInt(request.getParameter("lesson-type"));
                boolean isStatus = Boolean.parseBoolean(status);

                CourseLesson c = new CourseLesson(m, courseid, lessonname, lessontopic, typeID, filename, lessoncontent, isStatus);
                u.AddLesson(c);
                request.setAttribute("course", courseid);

                response.sendRedirect("sublist");
            }
           else if (lessonname != null && videolink!=null) {
                int typeID = Integer.parseInt(request.getParameter("lesson-type"));
                boolean isStatus = Boolean.parseBoolean(status);

                CourseLesson c = new CourseLesson(m, courseid, lessonname, lessontopic, typeID, videolink, lessoncontent, isStatus);
                u.AddLesson(c);

                response.sendRedirect("sublist");

            } else {
                request.setAttribute("course", courseid);

                request.getRequestDispatcher("AddNewLesson.jsp").forward(request, response);

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
