/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.CourseDAO;
import DAO.SliderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import model.CourseCatergory;
import model.Slider;

/**
 *
 * @author Admin
 */
@MultipartConfig(maxFileSize = 16177216)
public class EditSlider extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditSliderServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditSliderServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        CourseDAO cat = new CourseDAO();
        List<CourseCatergory> category = cat.getAllCa();
        SliderDAO dao = new SliderDAO();
        Slider slider = dao.selectByID(id);
        request.setAttribute("slider", slider);
        request.setAttribute("category", category);
        request.getRequestDispatcher("editSlider.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SliderDAO dao = new SliderDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        String catname = request.getParameter("catname");
        String backlink = request.getParameter("backlink");
        String filename;
        try{
        //Upload photo
        Part part = request.getPart("image");
        String realPath = "C:\\Users\\admin\\Desktop\\SWP391\\summer2023-swp391.se1714-g3\\OnlineLearning\\web\\assets\\img";
//        String realPath = request.getServletContext().getRealPath("/image/");
        String submittedFileName = part.getSubmittedFileName();
        filename = Paths.get(submittedFileName).getFileName().toString();
        if (!Files.exists(Paths.get(realPath))) {
            Files.createDirectory(Paths.get(realPath));
        }
        part.write(realPath + "/" + filename);
        filename = "assets/img/" + filename;
//        out.println(filename);
        }catch(Exception e){
            SliderDAO u = new SliderDAO();
            Slider s = u.selectByID(id);
            filename = s.getUrlImage();
            
        }
        String status = request.getParameter("status");
        String title = request.getParameter("title");
        String note = request.getParameter("note");
        dao.updateSlider(id, filename, catname,status,backlink,note,title);
        response.sendRedirect("SliderList");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
