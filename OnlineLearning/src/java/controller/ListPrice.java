/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAO.PriceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Price;

/**
 *
 * @author dell
 */
@WebServlet(name = "ListPrice", urlPatterns = {"/listprice"})
public class ListPrice extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListPrice</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListPrice at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

                PriceDAO p = new PriceDAO();

        String action = request.getParameter("action");

        if (action == null) {
            List<Price> lprice = p.getAll();
            int page, numperpage = 6;
            int size = lprice.size();
            int num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);//so trang
            String xpage = request.getParameter("page");
            if (xpage == null) {
                page = 1;
            } else if (!xpage.matches("[0-9]+")) {
                response.sendRedirect("forbiddenAlert.jsp");
                return;
            } else {
                page = Integer.parseInt(xpage);
            }
            if (page > 10) {
                response.sendRedirect("forbiddenAlert.jsp");
                return;
            }
            int start, end;
            start = (page - 1) * numperpage;
            end = Math.min(page * numperpage, size);
            List<Price> price = p.getListByPage(lprice, start, end);

            request.setAttribute("page", page);
            request.setAttribute("num", num);
            request.setAttribute("lprice", price);
            request.setAttribute("check", "list");
            request.getRequestDispatcher("ListPrice.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                PriceDAO p = new PriceDAO();

        String action = request.getParameter("action");

        if (action == null) {
            List<Price> lprice = p.getAll();
            int page, numperpage = 6;
            int size = lprice.size();
            int num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);//so trang
            String xpage = request.getParameter("page");
            if (xpage == null) {
                page = 1;
            } else if (!xpage.matches("[0-9]+")) {
                response.sendRedirect("forbiddenAlert.jsp");
                return;
            } else {
                page = Integer.parseInt(xpage);
            }
            if (page > 10) {
                response.sendRedirect("forbiddenAlert.jsp");
                return;
            }
            int start, end;
            start = (page - 1) * numperpage;
            end = Math.min(page * numperpage, size);
            List<Price> price = p.getListByPage(lprice, start, end);

            request.setAttribute("page", page);
            request.setAttribute("num", num);
            request.setAttribute("lprice", price);
            request.setAttribute("check", "list");
            request.getRequestDispatcher("ListPrice.jsp").forward(request, response);
    }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
