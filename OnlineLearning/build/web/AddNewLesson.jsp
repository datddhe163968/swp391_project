<%-- 
    Document   : MyRegistrations
    Created on : May 23, 2023, 3:22:29 PM
    Author     : quang
--%>

<%@ page import="java.util.List" %>
<%@page import= "model.CourseLesson" %>
<%@page import= "DAO.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%    
Integer courseid = (Integer) request.getAttribute("courseid");

    
    LessonDAO u = new LessonDAO();


    List<CourseLesson>  lesson1 = (List<CourseLesson>) request.getAttribute("lesson");

    // Thực hiện các lệnh khác ở đây nếu lessonList đã được thiết lập

   
%>
<html>
    <head>
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <title>Subjects List</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">


        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <style>

        </style>
        <link rel="stylesheet" type="text/css" href="assets/css/lessondetail.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.15/dist/sweetalert2.min.css">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.15/dist/sweetalert2.min.js"></script>

    </head>
    <%if(request.getSession().getAttribute("user")==null)
               response.sendRedirect("forbiddenAlert.jsp");
    %>
    <c:if test="${sessionScope.roleName.equals('admin') || sessionScope.roleName.equals('expert')}">
        <body>
            <%@include file="header.jsp" %>
            <main class="ttr-wrapper">
                <div class="container-fluid">
                    <div class="container2">
                        <span class="navbar3-brand"><a href="home">Home</a></span>
                        <span class="navbar3-brand-divider ">/</span>
                        <span class="navbar3-brand"><a href="sublist">Subject Management</a></span>
                        <span class="navbar3-brand-divider ">/</span>
                        <span class="navbar3-brand"><a href="sublesson?courseid=${course}">Lessons Management</a></span>
                        <span class="navbar3-brand-divider ">/</span>
                        <span class="navbar3-brand"><a href="AddNewLesson.jsp">Add New Lesson</a></span>
                    </div>
                </div>
                <div class="row">
                    <!-- Your Profile Views Chart -->
                    <div class="col-lg-12 m-b30">
                        <div class="widget-box d-flex justify-content-center">
                            <div class="hidden w-50 card p-3 m-3" id="hidden" >
                                <div class="manage" style="text-align: center;">
                                    <h1>Add New Lesson </h1>
                                    <!--                                    <button class="add-subject">Add new Subject</button>-->
                                </div>

                                <div>
                                    <form action="add" method="post" onsubmit="return validateForm()"  enctype="multipart/form-data" >
                                        <div>
                                            <label>Lesson Type:</label>
                                            <select name="lesson-type" onchange="toggleVideoLink()">
                                                <option value="1" >Video</option>
                                                <option value="2">HTML</option>
                                            </select>

                                        </div>
                                        <div>
                                            <label>Status:</label>
                                            <select name="status" >
                                                <option value="true" >Active</option>
                                                <option value="false">Deactive</option>

                                            </select>
                                        </div>
                                        <div  id="videolink-div">
                                            <label>Video Link:</label>
                                            <input type="text" name="videolink"   oninput="if (this.validity.patternMismatch) this.setCustomValidity('Vui lòng đúng định dạng: https://www.youtube.com/embed/'); else this.setCustomValidity('')" pattern="https:\/\/www.youtube.com\/embed\/.+">
                                        </div>
                                        <div id="htmlfile-div" style="display:none">
                                            <label>HTML File:</label>
                                            <input type="file" name="html" accept=".html" onchange="checkFileSize(event)" maxlength="10000000" >
                                        </div>
                                        <div class="card-courses-title">
                                            <label>Lesson Name:</label>
                                            <input type="text" name="lessonname"  required >
                                        </div>
                                        <div class="card-courses-list-bx">
                                            <div class="card-courses-user-info">
                                                <label>Lesson Topic:</label>
                                                <input type="text" name="lessontopic"  " required>
                                                <input type="hidden" name="courseid"  value="${course}">


                                            </div>
                                        </div>
                                        <div class="card-courses-list-bx"></div>
                                        <div class="row card-courses-dec">
                                            <div class="col-md-12">
                                                <label>Lesson Description:</label>
                                            </div>
                                        </div>
                                        <div class="row card-courses-dec">
                                            <div class="card-courses-user-info">
                                                <textarea name="lessoncontent" maxlength="1000" > </textarea >
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between align-content-center">
                                            <button class="btn btn-danger" onclick="window.location.href='sublesson?courseid=${course}'">Cancel</button>
                                            <button class="btn btn-success" type="submit" name="action" value="save">Submit</button>
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- Your Profile Views Chart END-->
                </div>
            </div>


        </main>

        <%@include file="footer.jsp" %>

    </body>
         <script src="assets/js/newlesson.js"></script>
   
</c:if>

</html>
