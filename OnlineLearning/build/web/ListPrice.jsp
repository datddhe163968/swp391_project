<%-- 
    Document   : Newsubject
    Created on : Jun 16, 2023, 8:47:59 PM
    Author     : dell
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List" %>
<%@page import = "model.*" %>
<%@page import = "java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    CourseDAO u = new CourseDAO();
    List<Course> lst = u.getCourses();
    int countCourse = u.countCourse();
    PriceDAO p = new PriceDAO();
    List<Price> lstp = p.getAll();
%>

<html>
    <head>
        <%if(request.getSession().getAttribute("user")==null)
                   response.sendRedirect("forbiddenAlert.jsp");
        %>
        <title>Price Packages</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" crossorigin="anonymous" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/detective.css">
        <link rel="stylesheet" href="assets/css/slidebar.css">
        <link rel="stylesheet" href="assets/css/hot.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    </head>


    <body>

        <%@include file="header.jsp"  %>
        <div class="code-section" style="margin-top: 20px;">
            <section class="cart_area">
                <div class="container">
                    <div class="table-title">
                        <div class="d-flex justify-content-between">
                            <div class="">
                                <h2>Manage Price</h2>
                            </div>
                            <div class="">
                                <a href="price" data-toggle="modal"> <span  class="btn btn-success">Add Price package</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="cart_inner">
                        <div class="table-responsive">
                            <table class="table mt-5 mb-5">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">PackageName</th>
                                        <th scope="col">Duration</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">ListPrice</th>
                                        <th scope="col">SalePrice</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <c:if test="${lprice != null}">

                                    <tbody>

                                        <c:forEach items="${lprice}" var="p">
                                            <tr> 
                                                <td>${p.getPackageId()}</td>
                                                <td>${p.getPackageName()}</td>
                                                <td>${p.getDuration()}</td>
                                                <td>${p.isStatus()}</td>
                                                <td>${p.getListPrice()}</td>
                                                <td>${p.getSalePrice()}</td>
                                                <td>
                                                    <p>
                                                        <a href="editprice?Id=${p.getPackageId()}" data-toggle="modal">
                                                            <i class="fas fa-edit"></i>
                                                        </a>    
                                                        <a class="text-danger" href="deleteprice?pid=${p.getPackageId()}" onclick="return confirm('Are you sure you want to delete?');">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </c:forEach>

                                    </tbody>

                                </c:if>
                            </table>
                            <c:if test="${requestScope.check.equals('list')}">
                                <nav class="blog-pagination justify-content-center d-flex">
                                    <ul class="pagination">
                                        <c:set var="page" value="${page}"/>
                                        <c:if test="${requestScope.page > 1}">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="listprice?page=${requestScope.page-1}">Previous</a></li>
                                            </c:if>
                                            <c:forEach begin="${1}" end="${num}" var="i">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="listprice?page=${i}">${i}</a></li>
                                            </c:forEach>
                                            <c:if test="${requestScope.num > requestScope.page}">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="listprice?page=${requestScope.page+1}">Next</a></li>
                                            </c:if>  
                                    </ul>
                                </nav>
                            </c:if>
                        </div>
                    </div>

                </div>
            </section>
        </div>                               
    </body>
    <%@include file="footer.jsp" %>
</html>

