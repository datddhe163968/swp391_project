<%-- 
    Document   : MyRegistrations
    Created on : May 23, 2023, 3:22:29 PM
    Author     : quang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="model.*"%> 
<%@page import="DAO.*"%> 
<%@page import = "java.util.*" %>

<%@page import = "model.Register" %>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.List" %>
<% RegisterDAO dao = new RegisterDAO();
 List<Register> list =(List<Register>)request.getAttribute("listRe");%>
 <%
        AccountDAO a = new AccountDAO();
        RegisterDAO r = new RegisterDAO();
        ArrayList<Account> lst = a.selectAll();
        %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <title>My Registrations</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">


        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">

        <!-- JS của Bootstrap (cần thiết cho các thành phần JavaScript của Bootstrap) -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
        <link rel="stylesheet" href="assets/css/myregi.css">
        <style>
  
        </style>
    </head>

        <body>

            <%@include file="header.jsp" %>
            <div class="navbar-link">
                <span><a href="home">Home</a></span>
                <span class="ms-2 me-2">/</span>
                <span><a href="#">My Registrations</a></span>
        </div>
            <h1  style="text-align: center;">My registrations</h1>
            <section class="body_content">

                <div class="container">
                    <div class="row">
                        <div style="">
                            <div class="search-box" >
                                <form action="search" method="get">
                                    <input type="hidden" name="userid" value="${user.getId()}">
                                    <input type="text" name="txtt" placeholder="search by course name...">
                                    <button class="btn btn-primary btn-success" type="submit">Search</button>
                                </form>
                            </div>

                            <button class="btn btn-primary btn-success" style="width: 150px;"  onclick="goToCourseList()">Add Course</button>
                            <form action="filterRegistration" method="get">
                                <div class="filter-box">
                                    <select name="dateFilter" id="date-filter" style="">
                                        <option value="0">All Dates</option>
                                        <option value="1">Last Week</option>
                                        <option value="2">Last Month</option>
                                        <option value="3">Last 3 Months</option>
                                        <option value="4">Last 6 Months</option>
                                    </select>
                                                            <input type="hidden"  name="user-id" value="${user.getId()}"  />

                                    <button class="btn btn-primary btn-success" type="submit" id="filter-button">Filter</button>
                                </div>
                            </form>



                            <div class="table_regis">

                             
                                <table class="table3" style="border:none">
                                      <thead>
                                    <tr>
                                        <th>ID</th>
                                         <th>Email</th>
                                        <th>Time</th>
                                        <th>Course</th>
                                        <th>Pakage</th>
                                        <th>Total Cost</th>
                                        <th>Valid from</th>
                                        <th>Valid to</th>
                                        <th>Update by</th>
                                        <td colspan="2"><i class="fa-solid fa-circle-info" ></i></td>

                                    </tr>
                                      </thead>
                                         <tbody>
                                    <% int count = 1; %>
                                    <% for(Register x: list) { %>
                                    <tr>
                                         <td><%= count %></td>
                                         <td><%= a.getUserEmail(x.getUserId()) %></td>
                                        <td><%= x.getValidFrom() %></td>
                                        <td><%= r.getCourseName(x.getCourseId()) %></td>
                                        <td>  <%= r.getPrice(x.getPackageId())%></td>
                                        <td><%= x.getCost() %></td>
                                        <td><%= x.getValidFrom() %></td>
                                        <td><%= x.getValidTo() %></td>
                                        <td><%= a.getUsername(x.getUserId()) %></td>
                                        <td><a class="text-success" href="LessonView.jsp?courseid=<%=x.getCourseId()%>"><i class="fa-solid fa-eye"></i></a></td>
                                            <td><a class="text-success" onclick="return confirm('Are you sure to cancel the course and we will not refund?')" href="test?courseid=<%=x.getCourseId()%>&user-id=<%=x.getUserId()%>">
                                                    <i class="fa-solid fa-trash"></i>
                                                </a>
                                            </td>
                                    </tr>
                                    <% count++; %>
                                    <% } %>
                                      </tbody>
                                </table>



                            </div>


                        </div>
                                      
                    </div>


            </section>
                                      <div class="page-patri" style="text-align: center;">

                                        <c:set var="page" value="${requestScope.page}"/>
                                       
                                        <div class="pagination" style="display: inline-block;">
                                            <c:choose>
                                               
                                                 <c:when test="${not empty txtSearch1}">
                                                     <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                                                        <c:choose>
                                                            <c:when test="${i == page}">
                                                                <a href="search?page=${i}&txtt=${txtSearch1}&userid=${user.getId()}" style="color: red;">${i}</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="search?page=${i}&txtt=${txtSearch1}&userid=${user.getId()}">${i}</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                   
                                                </c:when>   
                                                                <c:when test="${not empty dateFilter}">
                                                     <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                                                        <c:choose>
                                                            <c:when test="${i == page}">
                                                                <a href="filterRegistration?page=${i}&dateFilter=${dateFilter}&user-id=${user.getId()}" style="color: red;">${i}</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="filterRegistration?page=${i}&dateFilter=${dateFilter}&user-id${user.getId()}">${i}</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                   
                                                </c:when>   
                                                <c:otherwise>
                                                    <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                                                        <c:choose>
                                                            <c:when test="${i == page}">
                                                                <a href="myregistation?page=${i}&user-id=${userid}" style="color: red;">${i}</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="myregistation?page=${i}&user-id=${userid}">${i}</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                    
                                                </c:otherwise>
                                            </c:choose>

                                </div>
                            </div>
            <%@include file="footer.jsp" %>

        </body>


    <script src="assets/js/Myregistation.js"></script>

</html>
