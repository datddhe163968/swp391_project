<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Signup</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">

        <!--Load fonts style after rendering the layout styles--> 
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
        <link rel="stylesheet" href="assets/css/main.css">

    </head>
    <body>
        <jsp:include page="header.jsp" />

        <section class="login_box_area section_gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="login_box_img">
                            <img class="img-fluid" src="assets/img/login.jpg" alt="">
                            <div class="hover">
                                <h4>Already have account?</h4>
                                <a class="btn btn-success" href="login.jsp">Login</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 align-content-center"> 
                        <div class="login_form_inner" style="padding: 30px 50px 10px;">
                            <h1 class="text-center">Sign up</h1>
                            <form action="signup" method="post">
                                <div class="form-group mb-3">
                                    <label for="fullname">Full Name: <span class="text-danger">${alertName}</span></label>
                                    <input type="text" id="fullname" name="fullname" value="${fullName}" class="form-control" required placeholder="e.g. Online Learning">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="username">Username: <span class="text-danger">${alertUserName}</span></label>
                                    <input type="text" id="username" name="username" value="${userName}" class="form-control" required placeholder="e.g. onlinelearning2023">
                                </div>
                                <div class="form-group mb-3">
                                    <div class="d-flex  justify-content-between">
                                        <label for="password">Password: <span class="text-danger">${alertPassword}</span></label>
                                        <i onclick="hide('password')" class="fa-solid fa-eye" style="cursor: pointer"></i>
                                    </div>
                                    <input type="password" id="password" name="password" class="form-control" required placeholder="e.g. 123456">
                                </div>
                                <div class="form-group mb-3">
                                    <div class="d-flex  justify-content-between">
                                        <label for="confirmPassword">Confirm Password: <span class="text-danger">${alertConfirmPassword}</span></label>
                                        <i onclick="hide('confirmPassword')" class="fa-solid fa-eye" style="cursor: pointer"></i>
                                    </div>

                                    <input type="password" id="confirmPassword" name="confirmPassword" class="form-control" required>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="email">Email: <span class="text-danger">${alertEmail}</span></label>
                                    <input type="email" id="email" name="email" value="${email}" class="form-control" required placeholder="e.g. onlinelearning2023@gmail.com">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="phoneNum">Phone Number: <span class="text-danger">${alertPhone}</span></label>
                                    <input type="text" id="phoneNum" name="phoneNum" value="${phoneNumber}" class="form-control" required placeholder="e.g. 0987654321">
                                </div>

                                <div class="d-flex justify-content-between">
                                    <div class="gender-radio">
                                        <label>Gender: </label>
                                        <label>
                                            <input type="radio" name="gender" value="true" checked=""> Male
                                        </label>
                                        <label>                                      
                                            <input type="radio" name="gender" value="false"> Female
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-success ">Sign Up</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="assets/js/password.js"></script>
    </body>
    <%@include file="footer.jsp" %>                                                                                                                                                                                                                                        
</html>

