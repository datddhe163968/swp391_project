<%-- 
    Document   : userprofile
    Created on : May 16, 2023, 11:48:37 PM
    Author     : quang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.*"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit profile</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <!--
        
        -->
    </head>
    <%
        Account user = (Account) request.getSession().getAttribute("user");
        if (user == null) {
            response.sendRedirect("forbiddenAlert.jsp");
        }
    %>
    <style>
        .info{
            width: 40%;
        }

    </style>
    <body>
        <!-- Start Header Area -->
        <jsp:include page="header.jsp" />
        <!-- End Header Area -->

        <div class="col mb-5" >
            <h1 class="text-center m-5">Edit profile</h1>
            <div class="d-flex justify-content-center align-items-center">
                <div class="m-2 shadow rounded p-5 h-100">
                    <div class="d-flex justify-content-center align-items-center"><img class="rounded-circle" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"><span> </span></div>
                </div>

                <div class="info m-2 shadow rounded p-5 h-100">
                    <form id="myForm" action="edit" method="post">
                        <div class="d-flex mb-3">
                            <h5 class="w-25">Username:</h5 >
                            <input class="ps-1 rounded w-75" name='userName' type='text' value="${sessionScope.user.getUserName()}">
                        </div>
                        <div class="d-flex mb-3">
                            <h5 class="w-25">Fullname:</h5 >
                            <input class="ps-1 rounded w-75" name='fullName' type='text' value="${sessionScope.user.getFullName()}">
                        </div>
                        <div class="d-flex mb-3">
                            <h5 class="w-25">Email:</h5 >
                            <input class="ps-1 rounded w-75" name='email' type='text' value="${sessionScope.user.getEmail()}">
                        </div>
                        <div class="d-flex mb-3">
                            <h5 class="w-25">Phone:</h5 >
                            <input class="ps-1 rounded w-75" name='phoneNumber' type='text' value="${sessionScope.user.getPhoneNum()}">
                        </div>
                        <div class="d-flex mb-3">
                            <h5 class="w-25">Address:</h5 >
                            <input class="ps-1 rounded w-75" name='Address' type='text' value="${sessionScope.user.getAddress()}">
                        </div>
                        <div class="d-flex mb-3">
                            <h5 class="w-25">Role:</h5 >
                            <input class="ps-1 rounded w-75" name='role' type='text' value="${sessionScope.user.getRoleName()}" readonly="">
                        </div>
                        <div class="d-flex mb-3">
                            <h5 class="w-25">Gender:</h5 >
                            <div class="d-flex justify-content-around w-100">
                                <c:if test="${sessionScope.user.getGender()}">
                                    <div><input type="radio" name="gender" value="true" checked> Male</div>

                                    <div><input type="radio" name="gender" value="false"> Female</div>
                                    </c:if>
                                    <c:if test="${!sessionScope.user.getGender()}">
                                    <div><input type="radio" name="gender" value="true"> Male</div>

                                    <div><input type="radio" name="gender" value="false" checked> Female</div>
                                    </c:if>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mt-3">
                            <button class="btn btn-primary btn-success" type='submit' form="myForm">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>

    <!-- End footer -->

    <!-- Start Script -->
    <script>
        function openCP() {
            document.querySelector(".change-password").style.display = "block";
        }
        function closeCP() {
            document.querySelector(".change-password").style.display = "none";
        }
        function checkVal() {
            var current_password = document.getElementById('current-password').value;
            var new_password = document.getElementById('new-password').value;
            var confirm_password = document.getElementById('confirm-password').value;
            var check = document.getElementById('check').value;
            console.log(current_password, new_password, confirm_password);
            if (current_password !== check) {
                console.log('1 false');
                document.getElementById('alert-current-password').innerText = 'Current password is not correct!';
                return false;
            } else {
                document.getElementById('alert-current-password').innerText = '';
            }
            if (!new_password.match(/^.{6,}$/)) {
                console.log('2 false');
                document.getElementById('alert-new-password').innerText = 'Password has to contains more or equal than 6 chars!';
                return false;
            } else {
                if (new_password === current_password) {
                    document.getElementById('alert-new-password').innerText = 'New password is the same as current password';
                    return false;
                } else {
                    document.getElementById('alert-new-password').innerText = '';
                }
            }
            if (confirm_password !== new_password) {
                console.log('3 false');
                document.getElementById('alert-confirm-password').innerText = 'Confirm not match new password!';
                return false;
            } else {
                document.getElementById('alert-confirm-password').innerText = '';
            }
        }
    </script>
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/templatemo.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- End Script -->
    <!-- Start footer -->
    <%@include file="footer.jsp" %>
</body>
</html>

