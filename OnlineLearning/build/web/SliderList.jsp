<%-- 
    Document   : 
    Created on : 
    Author     : Dung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Slider List</title>
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" crossorigin="anonymous" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/detective.css">
        <link rel="stylesheet" href="assets/css/slidebar.css">
        <link rel="stylesheet" href="assets/css/hot.css">
        <link rel="stylesheet" href="assets/css/Slider.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Load fonts style after rendering the layout styles -->






    </head>
    <%if(request.getSession().getAttribute("user")==null)
                   response.sendRedirect("forbiddenAlert.jsp");
    %>
    <c:if test="${sessionScope.roleName.equals('marketer')}">
        <body>    

            <!-- Start Header Area -->
            <%@ include file = "header.jsp" %>
            <!-- End Header Area -->

            <!--================Slider Area =================-->




            <section class="cart_area mt-5 ">
                <div class="container">
                    <div class="table-title">
                       
                        <div class="row mt-3">
                            <div class="col-sm-5">
                                <h1>Slider List</h1>
                            </div>
                           
                        </div>
                    </div>
                    <div class="d-flex justify-content-between">
                          <form action="FilterSlider" method="get" style="width: 250px">
                            <select class="col-6" name="filterstatus" onchange="this.form.submit()">
                                <option value="" disabled selected hidden>Status</option>
                                <option value="">All</option>
                                <option value="true">Show</option>
                                <option value="false">Hide</option>
                            </select>
                        </form>
                         <div>
                                <a href="AddSlider" data-toggle="modal"> <span  class="btn btn-success">Add New Slider</span></a>
                            </div>
                    </div>
                   
                    <div class="cart_inner">
                        <div class="box">
                            <form class="sbox" action="SliderList" method="POST">
                                <input class="stext" type="text" name="SlideSearch" placeholder="Enter title...">
                                <a class="sbutton" type="submit" href="javascript:void(0);">
                                    <button class="fa fa-search" type="submit"></button>
                                </a>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Category</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listS}" var="s">
                                        <tr>
                                            <td>${s.getId()}</td>
                                            <td><img src="${s.urlImage}" alt="error" style="width: 100px"></td>
                                            <td>${s.getCategory()}</td>
                                            <td>${s.getTitle()}</td>
                                            <td>${s.isStatus()}</td>
                                            <td><a href="EditSlider?id=${s.getId()}" data-toggle="modal">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="DeleteSlider?id=${s.getId()}" onclick="return confirm('Are you sure you want to delete these Slider?');" >
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>

                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <c:if test="${requestScope.check.equals('list')}">

                                <nav class ="d-flex justify-content-center mb-4">
                                    <ul class="pagination">
                                        <c:set var="page" value="${page}"/>
                                        <c:if test="${requestScope.page > 1}">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="SliderList?page=${requestScope.page-1}">Previous</a></li>
                                            </c:if>
                                            <c:forEach begin="${1}" end="${num}" var="i">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="SliderList?page=${i}">${i}</a></li>
                                            </c:forEach>
                                            <c:if test="${requestScope.num > requestScope.page}">
                                            <li class="page-item""><a class="page-link ${i==page?"current":""}" href="SliderList?page=${requestScope.page+1}">Next</a></li>
                                            </c:if>
                                    </ul>
                                </nav>
                            </c:if>

                            <c:if test="${requestScope.check.equals('filter')}">

                                <nav class ="d-flex justify-content-center mb-4">
                                    <ul class="pagination">
                                        <c:set var="page" value="${page}"/>
                                        <c:if test="${requestScope.page > 1}">
                                            <li class="page-item" ><a class="page-link ${i==page?"current":""}" href="FilterSlider?filterstatus=${requestScope.filterstatus}&page=${requestScope.page-1}">Previous</a></li>
                                            </c:if>
                                            <c:forEach begin="${1}" end="${num}" var="i">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="FilterSlider?filterstatus=${requestScope.filterstatus}&page=${i}">${i}</a></li>
                                            </c:forEach>
                                            <c:if test="${requestScope.num > requestScope.page}">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="FilterSlider?filterstatus=${requestScope.filterstatus}&page=${requestScope.page+1}">Next</a></li>
                                            </c:if>
                                    </ul>
                                </nav>
                            </c:if>

                            <c:if test="${requestScope.check.equals('search')}">

                                <nav class ="d-flex justify-content-center mb-4">
                                    <ul class="pagination">
                                        <c:set var="page" value="${page}"/>
                                        <c:if test="${requestScope.page > 1}">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="SliderList?SlideSearch=${requestScope.SlideSearch}&page=${requestScope.page-1}">Previous</a></li>
                                            </c:if>
                                            <c:forEach begin="${1}" end="${num}" var="i">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="SliderList?SlideSearch=${requestScope.SlideSearch}&page=${i}">${i}</a></li>
                                            </c:forEach>
                                            <c:if test="${requestScope.num > requestScope.page}">
                                            <li class="page-item"><a class="page-link ${i==page?"current":""}" href="SliderList?SlideSearch=${requestScope.SlideSearch}&page=${requestScope.page+1}">Next</a></li>
                                            </c:if>
                                    </ul>
                                </nav>
                            </c:if>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Start footer -->
            <%@include file="footer.jsp" %>
            <!-- End footer -->

            <!-- Start Script -->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
            <!-- End Script -->
        </body>
    </c:if>
</html>
