<%-- 
    Document   : AddUser
    Created on : Jul 14, 2023, 9:32:51 PM
    Author     : dell
--%>

<%@page import = "java.util.*" %>
<%@page import = "DAO.*" %>
<%@page import = "model.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>

        <%if(request.getSession().getAttribute("user")==null){
                   response.sendRedirect("forbiddenAlert.jsp");
            }else {
        Account user = (Account) request.getSession().getAttribute("user");
            if(user.getRoleId()!= 3){
        response.sendRedirect("forbiddenAlert.jsp");
        }
            }
        %>
        <title>Add User</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/detective.css">
        <link rel="stylesheet" href="assets/css/slidebar.css">
        <link rel="stylesheet" href="assets/css/hot.css">
        <link rel="stylesheet" href="assets/css/flag.css">
        <link rel="stylesheet" href="assets/css/home.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
        <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    </head>
    <body>
    <body class="sb-nav-fixed">

        <jsp:include page="header.jsp"/>
        <div id="layoutSidenav">

            <div class="container-fluid rounded bg-white mt-5 mb-5">
                <div class="col">

                    <div class="border-right d-flex justify-content-center align-items-center ">

                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-center align-items-center mb-3">
                                <h4 class="text-right">Add Account</h4>
                            </div>

                            <div class ="row">
                                <div class="col-md-12">
                                </div>      
                            </div>

                            <form action="adduser" method="get">
                                <div class="row mt-3">
                                    <div class="col-md-12"><label for = "pwd">Username <span class="text-danger">${alertUserName}</span></label><input name="username" type="text" class="form-control" placeholder="Enter username" required></div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12"><label for = "pwd">Password <span class="text-danger">${alertPassword}</label><input id="password" name="password" type="password" class="form-control" placeholder="Enter password" required></div>
                                    <i onclick="hide()" class="fa-solid fa-eye" style="cursor: pointer"></i>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12"><label for = "pwd">Fullname</label><input name="fullname" type="text" class="form-control" placeholder="Enter Fullname" required></div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12"><label for = "pwd">Gender</label></div>
                                </div>
                                <div class="row mt-3 ml-4">
                                    <div class="col-md-5"> 
                                        <input  type="radio" name="gender" id="radio1" value="male">Male
                                    </div>

                                    <div class="col-md-5">
                                        <input type="radio" name="gender" id="radio2" value="female">Female
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12"><label for = "pwd">Address</label><input name="address" type="text" class="form-control" placeholder="Address" required></div>
                                    </div>    
                                    <div class="col-md-12 mt-3"><label for = "pwd">Email <span class="text-danger">${alertEmail} </label><input name="email" value="${email}" type="email" class="form-control" placeholder="Enter email" required></div>
                                    <div class="col-md-12 mt-3"><label for = "pwd">Phone Number <span class="text-danger">${alertPhone}</label><input name ="phonenum" value="${phoneNumber}" type="tel" class="form-control" placeholder="Enter phone number" required>
                                        <span id="phoneNumError" style="color: red; display: none;">Phone number must have exactly 10 digits.</span>
                                    </div>
                                    <div class="col-md-12 mt-3"><label for = "pwd">Role</label>
                                        <select class="form-select" name="roleId" id="role">
                                            <option value="1">Customer</option>
                                            <option value="2">Seller</option>
                                            <option value="3">Admin</option>
                                            <option value="4">Expert</option>
                                            <option value="5">Marketing</option>
                                        </select>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12"><label for = "pwd">Status</label></div>
                                    </div>
                                    <div class="col-md-5"> 
                                        <input  type="radio" name="statusId" id="radio1" value="1">Active
                                    </div>
                                    <div class="col-md-5">
                                        <input type="radio" name="statusId" id="radio2" value="2">Inactive
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 mt-5 text-center">
                                        <button class="btn btn-primary btn-success" type="submit">Add</button>

                                        <input class="btn btn-secondary" type="reset" value="Reset">
                                        <button type="button" class="btn btn-light"><a href="userlist">Back</a></button>
                                    </div>
                                </div>


                            </form>
                        </div>

                    </div>

                </div>
            </div>  
        </div>
        <script>
            function hide() {
                var passwordElement = document.getElementById("password");
                var eyeIcon = document.querySelector("i");
                console.log(eyeIcon);
                if (passwordElement.type === 'text') {
                    eyeIcon.classList.toggle("fa-eye");
                    eyeIcon.classList.toggle("fa-eye-slash");
                    passwordElement.type = 'password';
                } else {
                    eyeIcon.classList.toggle("fa-eye");
                    eyeIcon.classList.toggle("fa-eye-slash");
                    passwordElement.type = 'text';
                }
            }
        </script>

        <script>
            document.querySelector('input[name="phonenum"]').addEventListener('blur', function () {
                // Lấy giá trị đã nhập trong trường "Phone Number"
                var phoneNumber = this.value;

                // Kiểm tra nếu số ký tự khác 10 hoặc không phải là số, hiển thị thông báo lỗi
                if (phoneNumber.length !== 10 || isNaN(phoneNumber)) {
                    document.getElementById('phoneNumError').style.display = 'block';
                } else {
                    document.getElementById('phoneNumError').style.display = 'none';
                }
            });
        </script>
        <script>
            // Check if the URL parameter 'success' is present and has a value of 'true'
            const urlParams = new URLSearchParams(window.location.search);
            const isSuccess = urlParams.get('success');

            // If 'success=true', show an alert
            if (isSuccess === 'true') {
                alert('User added successfully!');
            }
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
    </body>
</body>
<jsp:include page="footer.jsp"/>

</html>
