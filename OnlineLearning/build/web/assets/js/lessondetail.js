/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


      var lessonType = '${lessondetail.lessonTypeId}';
      var videolinkDiv = document.getElementById("videolink-div");

      if (lessonType == 2 || lessonType == 3) {
        videolinkDiv.style.display = "none";
      } else {
        videolinkDiv.style.display = "block";
      }
   function previewImage(event) {
    var input = event.target;
    var reader = new FileReader();
    var preview = document.getElementById('preview');

    if (input.files && input.files[0]) {
      reader.onload = function() {
        preview.src = reader.result;
      };
      reader.readAsDataURL(input.files[0]);
    } else {
      preview.src = '${detail.image}';
    }
  }
      var subjectlist = document.getElementById("subjectlist");
      var hidden = document.getElementById("hidden");
      var editButton = document.querySelector(".edit-lesson");

      editButton.addEventListener("click", function() {
        subjectlist.style.display = "none";
        hidden.style.display = "block";
      });
        function checkFileSize(event) {
            var file = event.target.files[0];
            if (file.size > 10000000) {
                alert("File HTML quá lớn, vui lòng chọn tệp tin HTML có kích thước nhỏ hơn 10MB.");
                event.target.value = "";
            }
        }
   

    function saveData() {
      // Check if all required fields are filled in
      if (!document.getElementsByName('lessonname')[0].value||!document.getElementsByName('html')[0].value || !document.getElementsByName('lessontopic')[0].value || !document.getElementsByName('lessoncontent')[0].value) {
        // Display a warning message using SweetAlert2
        Swal.fire({
          title: 'HTML not empty!',
          icon: 'warning',
          showConfirmButton: false,
          timer: 2000
        });
        // Prevent the form submission
        return false;
      }

      // Save data to the database

      // Display success message using SweetAlert2
      Swal.fire({
        title: 'Data saved successfully!',
        icon: 'success',
        showClass: {
          popup: 'animate__animated animate__fadeInDownBig'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUpBig'
        },
        timer: 2000,
        timerProgressBar: true,
        showConfirmButton: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
          setTimeout(() => {
            Swal.resumeTimer()
          }, 200)
        }
      });
       document.querySelector('button[name="action"][value="save"]').disabled = true;

      // Wait for 5 seconds before submitting the form
      setTimeout(() => {
        // Submit the form
        document.querySelector('form').submit();
      }, 2000);

      // Prevent the form submission immediately
      return false;
    }
