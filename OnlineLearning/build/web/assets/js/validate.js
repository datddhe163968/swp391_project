function validateDescription(input) {
    const description = input.value;
    const wordCount = description.trim().split(/\s+/).length;

    if (wordCount > 1000) {
        input.setCustomValidity("Please enter a description with up to 1000 words.");
    } else {
        input.setCustomValidity("");
    }
}




function validateImage(input) {
    const file = input.files[0];
    const validExtensions = ["png", "jpg"];

    if (file) {
        const extension = file.name.split('.').pop().toLowerCase();
        if (!validExtensions.includes(extension)) {
            document.getElementById("imageValidationMessage").textContent = "Please select a PNG or JPG image.";
            input.value = ""; // Clear the selected file
        } else {
            document.getElementById("imageValidationMessage").textContent = "";
        }
    }
}