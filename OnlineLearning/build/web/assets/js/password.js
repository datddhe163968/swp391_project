/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

function hide(id) {
    var passwordElement = document.getElementById(id);
    var eyeIcon = document.querySelector("i");
    if (passwordElement.type === 'text') {
        passwordElement.type = 'password';
    } else {
        passwordElement.type = 'text';
    }
}
