<%-- 
    Document   : AddNewSubject
    Created on : Jun 18, 2023, 8:59:23 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import = "java.util.*" %>
<%@page import = "DAO.*" %>
<%@page import = "model.*" %>
<!DOCTYPE html>
<%
    // Check if the "addSuccess" attribute exists in the session and is true
    Boolean addSuccess = (Boolean) request.getSession().getAttribute("addSuccess");
    if (addSuccess != null && addSuccess) {
        // If the "addSuccess" attribute is true, show the success alert
        out.println("<script>alert('Subject added successfully!');</script>");

        // Set the "addSuccess" attribute to null to avoid showing the alert again on page refresh
        request.getSession().setAttribute("addSuccess", null);
    }
%>
<html lang="en">
    <head>

        <%if(request.getSession().getAttribute("user")==null)
                   response.sendRedirect("forbiddenAlert.jsp");
        %>
        <title>Add new Subject</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">

        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/detective.css">
        <link rel="stylesheet" href="assets/css/slidebar.css">
        <link rel="stylesheet" href="assets/css/hot.css">
        <link rel="stylesheet" href="assets/css/flag.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
        <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>




        <%@include file="header.jsp" %>
    <body class="sb-nav-fixed">

        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div id="editEmployeeModal">
                <div class="modal-dialog">
                    <div class="modal-content">


                        <form action="addsubject" method="post" enctype='multipart/form-data'>  

                            <div class="modal-header">						
                                <h3 class="text-success">New Subject</h3>
                            </div>
                            <div class="modal-body">	

                                <form class="row g-3">
                                    <div class="col-md-12">
                                        <div class="form-group mb-3">
                                            <label>Course Name</label>
                                            <input class="form-control" name="content" type="text" pattern="^[a-zA-Z0-9][a-zA-Z#0-9\s]*$" required>                               
                                        </div>    
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mb-3">
                                            <label>Description</label>
                                            <textarea name="Description" class="form-control" id="Description" rows="5" placeholder="Write product description here" required oninput="validateDescription(this)"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mb-3">
                                            <label for="price">Price:</label>
                                            <input type="text" name="price" class="form-control" step="0.01" min="0" onkeypress="return isNumber(event)" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="post_tag mb-3">
                                            <h4>
                                                <select class="form-select active" name="status" required>
                                                    <option value="" disabled selected hidden>Status</option>
                                                    <c:set var="trueValueRendered" value="false" />
                                                    <c:set var="falseValueRendered" value="false" />

                                                    <c:forEach items="${coursestatus}" var="s">
                                                        <c:if test="${s.isStatus() && trueValueRendered eq 'false'}">
                                                            <option value="${s.Status()}">Active</option>
                                                            <c:set var="trueValueRendered" value="true" />
                                                        </c:if>
                                                        <c:if test="${!s.isStatus() && falseValueRendered eq 'false'}">
                                                            <option value="${s.Status()}">Inactive</option>
                                                            <c:set var="falseValueRendered" value="true" />
                                                        </c:if>
                                                    </c:forEach>
                                                </select>



                                            </h4>
                                        </div>        
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mb-3">         
                                            <label for="avatar">Image</label>
                                            <input type="file" name="avatar" accept=".png, .jpg" onchange="validateImage(this)"  required><br>
                                            <small id="imageValidationMessage"></small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="post_tag mb-3">
                                            <select name="cat" required class="form-select">
                                                <option value="" disabled selected hidden>Choose category</option>
                                                <c:forEach items="${listCatergory}" var="l">                                              
                                                    <option value="${l.getCatergoryId()}">${l.getCatergoryName()}</option>
                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">                
                                    <div class="form-group" required>
                                        <label for="featured">Featured flag:</label>
                                        <select id="featured" name="featureflag" class="form-control">

                                            <option value="0">False</option>
                                            <option value="1">True</option>

                                        </select>
                                    </div>                            
                                        </div>




                            </div>
                            <div class="modal-footer d-flex justify-content-between">
                                <a href="sublist"><button type="button" class="btn btn-danger">Back</button></a>
                                <input type="submit" class="btn btn-success" value="Save" onclick="showSuccessMessage()">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

        <script src="assets/js/validate.js"></script>
        <script src="<%=request.getContextPath()%>/js/manager.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
        <script>
                                    function isNumber(evt) {
                                        evt = (evt) ? evt : window.event;
                                        var charCode = (evt.which) ? evt.which : evt.keyCode;
                                        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                            return false;
                                        }
                                        return true;
                                    }
        </script>

        <script>
            function showSuccessMessage() {
                alert("Add subject ?");
            }
        </script>
    </body>
    <%@include file="footer.jsp" %>
</html>
