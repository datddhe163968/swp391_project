<%-- 
    Document   : EditPrice
    Created on : Jul 13, 2023, 1:25:05 AM
    Author     : dell
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%if(request.getSession().getAttribute("user")==null)
                   response.sendRedirect("forbiddenAlert.jsp");
        %>
        <title>Nhập hoặc chỉnh sửa thông tin kích thước chủ đề</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/detective.css">
        <link rel="stylesheet" href="assets/css/slidebar.css">
        <link rel="stylesheet" href="assets/css/hot.css">
        <link rel="stylesheet" href="assets/css/coursecontent.css">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


    </head>
    <%@include file="header.jsp" %>
    <body>
        <div class="container1">
            <div class="container">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="editEmployeeModal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <form action="editprice" method="post">  
                                <div class="modal-header">						
                                    <h2 class="text-warning">Edit price</h2>
                                </div>
                                <div class="modal-body">	
                                    <div class="form-group">
                                        <!--<label>Price Name</label>
                                        <input name="price" type="text" pattern="^[a-zA-Z0-9][a-zA-Z0-9\s]*$" required> 
                                        -->
                                        <label>Price Name</label>
                                        <input value="${price.getPackageName()}" name="pname" type="text" pattern="^[a-zA-Z0-9][a-zA-Z0-9\s]*$">
                                    </div>   
                                    <div>
                                        <label>Duration (in days)</label>
                                        <input value="${price.getDuration()}" name="pduration" type="text">
                                    </div>
                                    <div class="post_tag">
                                        <h4>
                                            <select class="form-select active" name="status">
                                                <option value="" disabled selected hidden>Status</option>
                                                <c:set var="trueValueRendered" value="false" />
                                                <c:set var="falseValueRendered" value="false" />
                                                <c:forEach items="${pricestatus}" var="p">
                                                    <c:if test="${p.isStatus() && trueValueRendered eq 'false'}">
                                                        <option value="${p.isStatus()}">${p.isStatus()}</option>
                                                        <c:set var="trueValueRendered" value="true" />
                                                    </c:if>
                                                    <c:if test="${!p.isStatus() && falseValueRendered eq 'false'}">
                                                        <option value="${p.isStatus()}">${p.isStatus()}</option>
                                                        <c:set var="falseValueRendered" value="true" />
                                                    </c:if>
                                                </c:forEach>

                                            </select>
                                        </h4>
                                    </div>   
                                    <div class="form-group">
                                        <label for="price">List Price:</label>
                                        <input value="${price.getListPrice()}" name="plistprice" id="listPriceInput" class="form-control" step="0.01" min="0" onkeypress="return isNumber(event)" required>
                                    </div>

                                    <input value="${price.getPackageId()}" name="Id" hidden="">

                                    <div class="form-group">
                                        <label for="price">Sale Price:</label>
                                        <input type="number" value="${price.getSalePrice()}" name="psaleprice" id="salePriceInput" class="form-control" step="0.01" min="0" onkeypress="return isNumber(event)" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="pdescription" class="form-control" id="Description" rows="5" required oninput="validateDescription(this)">${price.getDescription()}</textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success" value="Save" onclick="showSuccessMessage()">
                                    <button type="button" class="btn btn-light"><a href="listprice">Back</a></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            const listPriceInput = document.getElementById('listPriceInput');
            const salePriceInput = document.getElementById('salePriceInput');

            // Function to validate the input fields
            function validatePrices() {
                const listPrice = parseFloat(listPriceInput.value);
                const salePrice = parseFloat(salePriceInput.value);

                if (listPrice <= salePrice) {
                    // Display an error message
                    alert('List price must be greater than sale price.');

                    // Clear the input fields
                    listPriceInput.value = '${price.getListPrice()}';
                    salePriceInput.value = '${price.getSalePrice()}';

                    // Set focus on the list price input field
                    listPriceInput.focus();

                    return;
                }


                return true; // Allow form submission
            }

            // Attach the validatePrices function to the form's submit event
            const form = document.querySelector('form');
            form.addEventListener('submit', validatePrices);
        </script>



    </body>
    <%@include file="footer.jsp" %>
</html>


