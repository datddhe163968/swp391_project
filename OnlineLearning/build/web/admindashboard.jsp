<%-- 
    Document   : admindashboard
    Created on : Jun 3, 2023, 8:37:15 PM
    Author     : dell
--%>

<%@page import = "java.util.*" %>
<%@page import = "DAO.*" %>
<%@page import = "model.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/logo.png">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/templatemo.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/newsubject.css">
        <link rel="stylesheet" href="assets/css/admin.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Load fonts style after rendering the layout styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Admin Dashboard</title>

        <link href="assets/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://kit.fontawesome.com/d846362117.css" crossorigin="anonymous">

        <script src="https://www.gstatic.com/charts/loader.js"></script>


        <script src="assets/js/scripts.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>

    </head>
    <%
        String dateFromStr = request.getParameter("dateFrom");
        String dateToStr = request.getParameter("dateTo");
        java.sql.Date dateFrom = null;
        java.sql.Date dateTo = null;

        if (dateFromStr != null && !dateFromStr.isEmpty() && dateToStr != null && !dateToStr.isEmpty()) {
          try {
            dateFrom = java.sql.Date.valueOf(dateFromStr);
            dateTo = java.sql.Date.valueOf(dateToStr);
          } catch (IllegalArgumentException e) {
            // Xử lý định dạng ngày không hợp lệ hoặc các ngoại lệ khác
            // Bạn có thể in/log thông báo lỗi hoặc thực hiện các hành động phù hợp
            e.printStackTrace();
          }
        }
    %>

    <%if(request.getSession().getAttribute("user")== null){
            response.sendRedirect("forbiddenAlert.jsp");  
    }else {
        Account user = (Account) request.getSession().getAttribute("user");
            if(user.getRoleId()!= 3){
        response.sendRedirect("forbiddenAlert.jsp");
        }
    }
    
            AccountDAO a = new AccountDAO();
            int countAccount = a.countAccount(); 
            
            CourseDAO c = new CourseDAO();
            int countCourse = c.countCourse();
            RegisterDAO r = new RegisterDAO();
            int countRegister = r.countRegister();  
            List<Account> name = a.getUsers();
            List<Course> lst = c.getCourses();
            List<Register> lrt = r.countCustomerCourse();
            List<Register> list = r.selectAll();
            List<Register> list1 = r.getRegistrationFormTo(dateFrom, dateTo);
    %>
    <%
//    int data = (int) request.getAttribute("countTime");
    %>
    <body class="sb-nav-fixed">

        <%@include file="header.jsp" %>
        <div>

            <div class="row">
                <main>
                    <div class="container-fluid ps-5 pe-5">
                        <h1 class="m-lg-5">Welcome To Dashboard</h1>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">Total Number of Customers</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <h3><%=countAccount%> Account</h3>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Total Number of Course</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <h3><%=countCourse%> Courses</h3>                                         
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body">Total Number of Registered</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <h3><%=countRegister%> Register</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Revenues</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <h3><%= a.countPrice()%>$</h3>
                                    </div>
                                </div>
                            </div>                                  
                        </div>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-area me-1"></i>
                                        Statics of new subjects 
                                    </div>                                    
                                    <div class="card-body subjects-table">                                                                                
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Subject name</th>
                                                    <th>Category</th>
                                                    <th>Customers</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% 
                                                    int currentPage = (request.getParameter("page") != null) ? Integer.parseInt(request.getParameter("page")) : 1;
                                                    int recordsPerPage = 4; // Số lượng mục trên mỗi trang
                                                    int start = (currentPage - 1) * recordsPerPage;
                                                    int end = Math.min(start + recordsPerPage, lst.size());
                                                    for (int i = start; i < end; i++) {
                                                        Course x = lst.get(i);
                                                %>
                                                <tr>
                                                    <td><%= x.getCourseName() %></td>
                                                    <td><%= c.getCategoryName(x.getCatergoryId()) %></td>
                                                    <td><%= r.countRegisterByCourse(x.getCourseId()) %></td>
                                                </tr>
                                                <% } %>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3">
                                                        <!-- Hiển thị nút phân trang -->
                                                        <div class="pagination ">
                                                            <% if (currentPage > 1) { %>
                                                            <a href="?page=<%= currentPage - 1 %>"><%= currentPage - 1 %></a>
                                                            <% } %>

                                                            <span class="current-page"><%= currentPage %></span>

                                                            <% if (currentPage * recordsPerPage < lst.size()) { %>
                                                            <a href="?page=<%= currentPage + 1 %>"><%= currentPage + 1 %></a>
                                                            <% } %>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        Top 3 New registrations
                                    </div>
                                    <div class="card-body subjects-table">                                                                                
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>UserID</th>
                                                    <th>RegisterTime</th>                                                  
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% for(Register e: lrt) { %>
                                                <tr>
                                                    <td><%= e.getUserId()%></td>
                                                    <td><%= e.getRegTime()%></td>
                                                </tr>
                                                <%}%>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-body"><canvas id="myPieChart" width="100%" height="50"></canvas></div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card mb-4">
                                    <div class="card-header">

                                        Trends of order count
                                    </div>
                                    <div class="card-body">
                                        <form action="admindashboard"  onsubmit="return validateForm()">
                                            <input type="hidden">
                                            <div class="d-flex align-items-center mb-3 mt-3">
                                                <div class="setDate setdatefrom">
                                                    <h7>Date From:</h7>
                                                    <input id="dateFrom" type="date" name="dateFrom" value="${requestScope.from}" />
                                                </div>
                                                <div class="setDate setdateto ms-5">
                                                    <h7>Date To:</h7>
                                                    <input id="dateTo" type="date" name="dateTo" value="${requestScope.to}"/>
                                                </div>
                                                <div>
                                                    <button class="btn btn-primary btn-success ms-5" type="submit">Show</button>
                                                </div>
                                            </div>
                                        </form>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>RegID</th>
                                                    <th>UserID</th>
                                                    <th>RegisterTime</th>  
                                                    <th>ValidFrom</th>  
                                                    <th>ValidTo</th>  
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${listregistraion}" var="b">
                                                    <tr>
                                                        <td>${b.getRegId()}</td>
                                                        <td>${b.getUserId()}</td>
                                                        <td>${b.getRegTime()}</td>     
                                                        <td>${b.getValidFrom()}</td>  
                                                        <td>${b.getValidTo()}</td>  
                                                    </tr>                                                   
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <c:if test="${requestScope.check.equals('list')}">
                                            <nav class="blog-pagination justify-content-center d-flex">
                                                <ul class="pagination">
                                                    <c:set var="page" value="${page}"/>
                                                    <c:if test="${requestScope.page > 1}">
                                                        <li class="page-item"><a class="page-link ${i==page?"current":""}" href="admindashboard?dateFrom=${requestScope.from}&dateTo=${requestScope.to}&page=${requestScope.page-1}">Previous</a></li>
                                                        </c:if>
                                                        <c:forEach begin="${1}" end="${num}" var="i">
                                                        <li class="page-item"><a class="page-link ${i==page?"current":""}" href="admindashboard?dateFrom=${requestScope.from}&dateTo=${requestScope.to}&page=${i}">${i}</a></li>
                                                        </c:forEach>
                                                        <c:if test="${requestScope.num > requestScope.page}">
                                                        <li class="page-item"><a class="page-link ${i==page?"current":""}" href="admindashboard?dateFrom=${requestScope.from}&dateTo=${requestScope.to}&page=${requestScope.page+1}">Next</a></li>
                                                        </c:if>  
                                                </ul>
                                            </nav>
                                        </c:if>
                                        </form>

                                    </div>

                                    <script>
                                        function validateForm() {
                                            var dateFrom = document.getElementById("dateFrom").value;
                                            var dateTo = document.getElementById("dateTo").value;

                                            if (dateFrom === "" || dateTo === "") {
                                                alert("Please select both start and end dates.");
                                                return false; // Prevent form submission
                                            }

                                            var startDate = new Date(dateFrom);
                                            var endDate = new Date(dateTo);

                                            if (startDate > endDate) {
                                                alert("Start date must be before end date.");
                                                return false; // Prevent form submission
                                            }

                                            return true; // Allow form submission
                                        }
                                    </script>



                                </div>

                                </main>
                              </div>
                        </div>
                        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
                        <script src="js/scripts.js"></script>
                        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
                        <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.min.js"></script>
                        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                        <script src="//code.jquery.com/jquery-1.12.4.js"></script>
                        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>







                        <%@include file="footer.jsp" %>

                        </body>

                        </html>
